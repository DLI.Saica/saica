﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Dapper;
using Microsoft.Data.SqlClient;
using MySql.Data.MySqlClient;
namespace Core.DB
{
    public class CustomSqlConnection : IDisposable
    {
        private IDbConnection _connection;
        private string connect;
        public CustomSqlConnection(string connectionString)
        {
            _connection = new MySqlConnection(connectionString);
            connect = connectionString;
        }

        public void Open()
        {
            ((MySqlConnection)_connection).Open();
        }

        public IDbConnection GetMySqlConnection()
        {
            IDbConnection _connection2= new MySqlConnection(connect);
            _connection2.Open();
            //_connection2.BeginTransaction();

            return _connection2;
        }
        public SqlMapper.GridReader QueryMultiple(string query, object param = null)
        {
            if (query != null && query.Contains("["))
            {
                query = query.Replace("[", "").Replace("]", "");
            }
            Debug.WriteLine(query);
            IDbConnection _connection2 = GetMySqlConnection();
            var results = _connection2.QueryMultiple(query, param);
            _connection2.Close();

            return results;

        }

        public IEnumerable<dynamic> Query(string query, object param = null, IDbTransaction transaction = null)
        {
            if (query != null && query.Contains("["))
            {
                query = query.Replace("[", "").Replace("]", "");
            }
            Debug.WriteLine(query);
            IDbConnection conn = GetMySqlConnection();
            
            var results = conn.Query(query, param, transaction);
            conn.Close();
            return results;

        }

        public IEnumerable<T> Query<T>(string query, object param = null, IDbTransaction transaction = null)
        {
            if (query != null && query.Contains("["))
            {
                query = query.Replace("[", "").Replace("]","");

            }

            Debug.WriteLine(query);
            IDbConnection conn = GetMySqlConnection();
            var results = conn.Query<T>(query, param, transaction);
            conn.Close();
            return results;

        }

        public IEnumerable<TThird> Query<T, TFirst, TSecond, TThird>(string query, Func<T, TFirst, TSecond, TThird> map,
            object param = null, IDbTransaction transaction = null)
        {
            if (query != null && query.Contains("["))
            {
                query = query.Replace("[", "").Replace("]", "");
            }
            IDbConnection conn = GetMySqlConnection();
            var results = conn.Query<T, TFirst, TSecond, TThird>(query, map, param, transaction);
            conn.Close();
            return results;

        }

        public IEnumerable<TSecond> Query<T, TFirst, TSecond>(string query, Func<T, TFirst, TSecond> map,
            object param = null, IDbTransaction transaction = null)
        {
            if (query != null && query.Contains("["))
            {
                query = query.Replace("[", "").Replace("]", "");
            }
            Debug.WriteLine(query);
            IDbConnection conn = GetMySqlConnection();
            var results = GetMySqlConnection().Query<T, TFirst, TSecond>(query, map, param, transaction);
            conn.Close();
            return results;

        }
        public int ExecuteScaler(string query, object param = null, IDbTransaction transaction = null)
        {
            if (query != null && query.Contains("["))
            {
                query = query.Replace("[", "").Replace("]", "");
            }
            Debug.WriteLine(query);
            IDbConnection conn = GetMySqlConnection();
            var x = conn.ExecuteScalar<int>(query, param, transaction);
            conn.Close();
            return x;
        }
        public int Execute(string query, object param = null, IDbTransaction transaction = null)
        {
            if(query != null && query.Contains("["))
            {
                query = query.Replace("[", "").Replace("]", "");
            }
            Debug.WriteLine(query);
            IDbConnection conn = GetMySqlConnection();
            var result = conn.Execute(query, param, transaction);
            conn.Close();
            return result;

        }


        public IDbTransaction BeginTransaction()
        {
            return _connection.BeginTransaction();
        }

        public void Dispose()
        {
            _connection.Dispose();
        }

        public void Close()
        {
            _connection.Close();
        }

        public ConnectionState State()
        {
            return _connection.State;
        }

        public bool IsAvailable()
        {
            return _connection.State != ConnectionState.Connecting && _connection.State != ConnectionState.Executing &&
                   _connection.State != ConnectionState.Fetching;
        }

        public bool IsClosed()
        {
            return _connection.State == ConnectionState.Closed;
        }
    }
}
