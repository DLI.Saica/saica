﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Repositories
{
    public class DimensionRepository : BaseIntRepository<Dimension>
    {
        public DimensionRepository()
        {
            base.Init("Dimension", "NameEn,Name,Icon,Color,DisplayOrder,CategoryId,IsActive");
        }

        public void DeleteAll()
        {
            Execute($"DELETE FROM {TableName};");
        }

        public List<SubCategory> GetByQueryPIE(String catId)
        {
            var data = Query<SubCategory>(" SELECT (select COLOR from dimension where Id = DimensionId) as color, Id, count(*) as Percentage , ( count(*)/(SELECT count(*) FROM AnnotationReason_report where CategoryId=" + catId+ ") *100 ) as Value ," +
" (select Name from dimension where Id = DimensionId) as Title," +
" (select NameEn from dimension where Id = DimensionId) as NameEn"+
" FROM AnnotationReason_report where CategoryId= " + catId+
" group by DimensionId ").ToList();
            return data;
        }

        public List<LineChart> GetByQueryLineDay(String DimintionId)
        {
            var data = Query<LineChart>(" SELECT (SELECT Name FROM dimension WHERE ID=" + DimintionId + " ) as name, count(*) as data, CONVERT((date(CreationDate)) , CHAR) as label FROM AnnotationReasonWord_report where AnnotationReasonId in (select id from annotationreason_report where IsDeleted=0 and DimensionId=" + DimintionId + ") group by date(CreationDate)" ).ToList();
            return data;
        }
        public List<LineChart> GetByQueryLineMonth(String DimintionId)
        {
            var data = Query<LineChart>(" SELECT (SELECT Name FROM dimension WHERE ID=" + DimintionId + " ) as name,  count(*) as data, EXTRACT( YEAR_MONTH FROM CreationDate) as label FROM AnnotationReasonWord_report where AnnotationReasonId in (select id from annotationreason_report where IsDeleted=0 and DimensionId=" + DimintionId + ") group by EXTRACT( YEAR_MONTH FROM CreationDate)").ToList();
            return data;
        }
        public List<LineChart> GetByQueryLineWeek(String DimintionId)
        {
            var data = Query<LineChart>(" SELECT (SELECT Name FROM dimension WHERE ID=" + DimintionId + " ) as name,  count(*) as data, CONCAT(WEEK(CreationDate),'-',EXTRACT( YEAR_MONTH FROM CreationDate)) as label FROM AnnotationReasonWord_report where AnnotationReasonId in (select id from annotationreason_report where IsDeleted=0 and DimensionId=" + DimintionId + ") group by CONCAT(WEEK(CreationDate),'-',EXTRACT( YEAR_MONTH FROM CreationDate)) ").ToList();
            return data;
        }
        public List<SubCategory> GetByWordCloud(String subCatId)
    {
        var data = Query<SubCategory>(" SELECT count(*) as Value, d.name as Title, (select count(*) from tweetword where wordId=d.id) as Percentage FROM AnnotationReason_report a, annotationreasonword_report b, tweetword c, word d " +
            " where a.DimensionId="+ subCatId + " and a.id = b.AnnotationReasonId and c.id = b.TweetWordId and d.id = c.TweetId and b.IsDeleted = 0 and a.IsDeleted = 0" +
            " group by d.name order by 1 desc " +
            " limit 0, 40 ").ToList();
        return data;
    }
        public List<SubCategory> GetByWordCloudUser(int userId)
        {
            var data = Query<SubCategory>("SELECT " + 
            "    count(*) as Value, d.name as Title, (select count(*) from tweetword where wordId = d.id) as Percentage " +
            "    FROM " +
            "    AnnotationReason_report a, " +
            "    Annotation_report ann, " +
            "    Annotationreasonword_report b, " +
            "    Tweetword c, " +
            "    word d, " +
            "    Annotationtaskusertweet tt " +
            "    WHERE " +
            "        a.Id = b.AnnotationReasonId " +
            "        AND c.Id = b.TweetWordId " +
            "        AND d.Id = c.TweetId " +
            "        AND ann.Id = a.AnnotationId " +
            "        AND tt.Id = ann.AnnotationTaskUserTweetId " +
            "        AND tt.status = 30 " +
            "        AND a.IsDeleted = 0 " +
            "        AND ann.IsDeleted = 0 " +
            "        AND b.IsDeleted = 0 " +
            "        AND tt.IsDeleted = 0 " +
            "        AND tt.userId=" +userId+
            " GROUP BY d.name " +
            " order by 1 desc " +
            " limit 0, 40").ToList();
            return data;
        }
        public List<SubCategory> GetByWordCloudUsers(String userIds)
        {
            var data = Query<SubCategory>("SELECT " +
            "    count(*) as Value, d.name as Title, (select count(*) from tweetword where wordId = d.id) as Percentage " +
            "    FROM " +
            "    AnnotationReason_report a, " +
            "    Annotation_report ann, " +
            "    Annotationreasonword_report b, " +
            "    Tweetword c, " +
            "    word d, " +
            "    Annotationtaskusertweet tt " +
            "    WHERE " +
            "        a.Id = b.AnnotationReasonId " +
            "        AND c.Id = b.TweetWordId " +
            "        AND d.Id = c.TweetId " +
            "        AND ann.Id = a.AnnotationId " +
            "        AND tt.Id = ann.AnnotationTaskUserTweetId " +
            "        AND tt.status = 30 " +
            "        AND a.IsDeleted = 0 " +
            "        AND ann.IsDeleted = 0 " +
            "        AND b.IsDeleted = 0 " +
            "        AND tt.IsDeleted = 0 " +
            "        AND tt.userId in (" + userIds + ") "+
            " GROUP BY d.name " +
            " order by 1 desc " +
            " limit 0, 40").ToList();
            return data;
        }
    }
/**/
public class Dimension : BaseIntModel
    {
        public string NameEn { get; set; }

        public string Name { get; set; }
        public string Icon { get; set; }
        public string Color { get; set; }
        public int DisplayOrder { get; set; }
        public int CategoryId { get; set; }
        public bool IsActive { get; set; }

    }
    public class SubCategory : BaseIntModel
    {
        public int Id { get; set; }
        public string NameEn { get; set; }
        public string Title { get; set; }
        public int Value { get; set; }
        public int Percentage { get; set; }
        public string color { get; set; }
    }
    public class LineChart : BaseIntModel
    {
        public int data { get; set; }
        public string label { get; set; }
        public string name { get; set; }
    }
    public class MasterLineChart : BaseIntModel
    {
        public int Id { get; set; }
        public String type { get; set; }
        public String name { get; set; }
        public bool howInLegend { get; set; }
        public List<LineChart> dataPoints { get; set; }
    }
}
