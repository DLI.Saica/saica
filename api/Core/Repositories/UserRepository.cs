using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json.Serialization;
using Core;
using Core.Repositories;

namespace Core.Repositories
{
    public class UserRepository : BaseIntRepository<User>
    {
        public UserRepository()
        {
            Init("User", "LastLogin,PasswordHash,PasswordSalt,Name,Username,Phone,Email,Role,IsActive");
        }

        public List<User> GetByOtherUsername(string value)
        {
            var data2 = Query<UserTweets>("SELECT * FROM annotationtask where userId="+value).ToList();
            List<User> data = new List<User>();
            for (int i = 0; i < data2.Count; i++)
            {
                List<User> dataTemp = Query<User>("SELECT * FROM user where id in ( select UserId from annotationtask where (StartTweetId between (" + data2[i].StartTweetId+ ") and ("+ data2[i].EndTweetId + ") or EndTweetId between (" + data2[i].StartTweetId + ") and (" + data2[i].EndTweetId + ")) and userId <>" + value+")").ToList();
                data.AddRange(dataTemp);
            }
            List<User> dupes = new List<User>();

            for (int i = 0; i < data.Count; i++)
            {
                var found = true;
                for (int x = 0; x < dupes.Count; x++)
                {
                       
                    if(data[i].Id == dupes[x].Id)
                    {
                            found = false;
                        break;
                    }
                }
                if (found)
                {
                    dupes.Add(data[i]);
                }
            }

            return dupes;
        }

        public List<UserData1> getUserData1(string value)
        {
            return Query<UserData1>("SELECT uss.name as name, " +
"    (SELECT COUNT(*) FROM annotationtaskusertweet alla WHERE alla.UserId = sn.userId and IsDeleted = 0 and alla.Status = 30) as noCompleted, " +
"    (SUM(sn.TaskDuration) / 60) as totalTime, "+
"    AVG(sn.TaskDuration) as avgTimeTweet, " +
"    (select AVG(ar.categoryId) from annotation_report ar, annotationtaskusertweet sn2 where ar.annotationtaskusertweetid = sn2.id and ar.IsDeleted = 0 and sn2.userid = sn.userId) as avgCountCategory, " +
"    (select AVG(ar.dimensionId) from annotation_report ar, annotationtaskusertweet sn2 where ar.annotationtaskusertweetid = sn2.id and ar.IsDeleted = 0 and sn2.userid = sn.userId) as avgCountDimention " +
" FROM annotationtaskusertweet sn, user uss " +
" WHERE sn.status = 30 AND uss.id = sn.userId GROUP BY uss.name ").ToList();
        }
        public List<UserData2> getUserData2(string value)
        {
            return Query<UserData2>("SELECT * FROM annotationtask where userId=" + value).ToList();
        }
        public List<UserData3> getUserData3(string value)
        {
            return Query<UserData3>("SELECT count(*) as noAnnotatedTweet, cat.name as CategoryAr, cat.nameEn as CategoryEn, dim.name as DimentionAr, dim.nameEn as DimentionEn "+
                " FROM annotationtaskusertweet sn, annotation_report anr, category cat, dimension dim "+
                " where sn.status = 30 and cat.id = anr.CategoryId " +
                " and dim.id = anr.dimensionId and sn.id = anr.AnnotationTaskId " +
                " group by cat.name, dim.name order by 2, 3 desc").ToList();
        }
        public List<UserData4> getUserData4(string value)
        {
            return Query<UserData4>("SELECT count(*) as countWord, w.Name as word FROM annotationreasonword_report r, word w where w.id = r.TweetWordId and r.IsDeleted=0 group by w.name order by 1 desc limit 150").ToList();
        }

        public List<LineChart> GetByQueryConfidence(int userId)
        {
            var data = Query<LineChart>(" SELECT (select name from user where id=" + userId + ") as name, " +
                "avg (LevelOfConfidenceId) as data  , " +
                "CONVERT((date(LastModified)) , CHAR) as label " +
                "FROM annotationtaskusertweet where userId=" + userId + " and status=30 group by date(LastModified)").ToList();
            return data;
        }

        public List<LineChart> GetByQueryTime(int userId)
        {
            var data = Query<LineChart>(" SELECT (select name from user where id=" + userId + ") as name, ," +
                "avg (TaskDuration) as data  , " +
                "CONVERT((date(LastModified)) , CHAR) as label " +
                "FROM annotationtaskusertweet where userId=" + userId + " and status=30 group by date(LastModified)").ToList();
            return data;
        }

        public List<LineChart> GetByQueryTweets(int userId)
        {
            var data = Query<LineChart>(" SELECT (select name from user where id=" + userId + ") as name, " +
                "count(*) as data  , " +
                "CONVERT((date(LastModified)) , CHAR) as label " +
                "FROM annotationtaskusertweet where userId=" + userId + " and status=30 group by date(LastModified)").ToList();
            return data;
        }

        public User GetByUsername(string value)
        {
            return GetSingle("Username", value.Trim());
        }

        public void CreateNewUserTables(User u)
        {
            var annotationTable = $@"CREATE TABLE  Annotation_{u.Id} (
                  Id   int AUTO_INCREMENT    NOT NULL,
               CreationDate datetime NULL,
                 LastModified datetime NULL,
                 IsDeleted tinyint(1) NULL,                
                CategoryId int  NULL,
                   DimensionId  int   NULL,
             AnnotationTaskUserTweetId  int,
              AnnotationTaskId  int  ,
            CONSTRAINT PK_Annotation{u.Id}  PRIMARY KEY 
                  (
             `Id` ASC
             )  
                )";


            var annotationReasonTable = $@"CREATE TABLE AnnotationReason_{u.Id} (
                     Id int  AUTO_INCREMENT NOT NULL,
                      CreationDate datetime  NULL,
                     LastModified datetime  NULL,
                    IsDeleted tinyint(1)  NULL,
                      AnnotationId int  NULL,
                     CategoryId    int NULL,
                    DimensionId int  NULL,
                      StartWordId int  NULL,
                     EndWordId int  NULL,
                      StartWordPosition int  NULL,
                    EndWordPosition int  NULL,
                    CONSTRAINT PK_AnnotationReason{u.Id}  PRIMARY KEY  
                    (
                      `Id` ASC
                   )  
                     )  ";


            var annotationReasonWordTable = $@"CREATE TABLE AnnotationReasonWord_{u.Id}(
                  Id   int  AUTO_INCREMENT NOT NULL,
                  CreationDate   datetime  NULL,
                  LastModified  datetime  NULL,
                  IsDeleted  tinyint(1) NULL,
                  AnnotationReasonId int  NULL,
                  TweetWordId   int  NULL,
                 CONSTRAINT  PK_AnnotationReasonWord{u.Id}  PRIMARY KEY  
                (
                  `Id` ASC
                )  
                  )  ";

            Execute("  BEGIN; "  + annotationTable);
            Execute("  BEGIN; " + annotationReasonTable);
            Execute("  BEGIN; " + annotationReasonWordTable);
            Console.WriteLine(annotationTable);
        }

    }
}

public class UserTweets : BaseIntModel
{
    public string Username { get; set; }
    public int StartTweetId { get; set; }
    public int EndTweetId { get; set; }
}


public class UserData1 : BaseIntModel
{
    public String name { get; set; }
    public double noCompleted { get; set; }
    public double totalTime { get; set; }
    public double avgTimeTweet { get; set; }
    public double avgCountCategory { get; set; }
    public double avgCountDimention { get; set; }
}
public class UserData2 : BaseIntModel
{
    public String name { get; set; }
    public double noCompleted { get; set; }
    public double totalTime { get; set; }
    public double avgTimeTweet { get; set; }
    public double avgCountCategory { get; set; }
    public double avgCountDimention { get; set; }
    public String weekly { get; set; }

}
public class UserData3 : BaseIntModel
{
    public double noAnnotatedTweet { get; set; }
    public String CategoryAr { get; set; }
    public String CategoryEn { get; set; }
    public String DimentionAr { get; set; }
    public String DimentionEn { get; set; }
}
public class UserData4 : BaseIntModel
{
    public String word { get; set; }
    public double countWord { get; set; }

}

public class User : BaseIntModel
{
    public string Name { get; set; }

    public string Username { get; set; }
    public string Password { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }
    public string PasswordHash { get; set; }
    public string PasswordSalt { get; set; }
    public DateTime? LastLogin { get; set; }
    public string Token { get; set; }

    public int Role { get; set; }
    public RoleEnum RoleEnum => (RoleEnum)Role;
    public string RoleName => RoleEnum.ToString();
    public bool IsActive { get; set; }

    public bool ChangePassword { get; set; }
    public int AvgLevelOfConfidence { get; set; }
    public int AvgLevelOfConfidence2 { get; set; }
    public int AvgTaskDuration { get; set; }
    public int TotalTasks { get; set; }
    public double Percentage { get; set; }
    public int TotalTaskDuration { get; set; }
    public int FinishedTasks { get; set; }
    public int RemainTasks { get; set; }
    public Core.Repositories.Annotations.UserStat statistics { get; set; }
    public double TotalCategory { get; set; }
    public double TotalDimension { get; set; }
    public double TotalReason { get; set; }
    public double avgTweetTime { get; set; }
    

    public bool Equals(User other)
    {
        return this.Id.Equals(other.Id);
    }



    public virtual void SetPassword(string passwordText)
    {
        if (string.IsNullOrWhiteSpace(passwordText))
            return;

        PasswordSalt = GenerateSalt();
        PasswordHash = Hash(passwordText, PasswordSalt);
    }
    public virtual string Hash(string value, string salt)
    {
        var hashed = Hash(Encoding.UTF8.GetBytes(value), Encoding.UTF8.GetBytes(salt));
        return Convert.ToBase64String(hashed);
    }

    public virtual bool VerifyPassword(string password)
    {
        if (string.IsNullOrEmpty(PasswordSalt) || string.IsNullOrEmpty(password))
            return false;
        var passwordHash = Hash(password, PasswordSalt);
        return PasswordHash.SequenceEqual(passwordHash);
    }
    public virtual byte[] Hash(string value, byte[] salt)
    {
        return Hash(Encoding.UTF8.GetBytes(value), salt);
    }

    public virtual byte[] Hash(byte[] value, byte[] salt)
    {
        byte[] saltedValue = value.Concat(salt).ToArray();
        return new SHA256Managed().ComputeHash(saltedValue);
    }
    private string GenerateSalt()
    {
        //var salt = Guid.NewGuid().ToByteArray();
        //var hashed = new SHA256Managed().ComputeHash(salt);
        var random = new RNGCryptoServiceProvider();
        var salt = new byte[32]; //256 bits
        random.GetBytes(salt);
        return Convert.ToBase64String(salt);
    }

}