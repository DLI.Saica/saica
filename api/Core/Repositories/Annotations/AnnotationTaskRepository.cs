﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Repositories.Annotations
{
    public class AnnotationTaskRepository : BaseIntRepository<AnnotationTask>
    {
        public AnnotationTaskRepository()
        {
            base.Init("AnnotationTask", "StartTweetId,EndTweetId,CreatedByUserId,UserId,Status,StartTime,FinishTime,TaskDuration");
        }


        public List<AnnotationTask> GetAllView(RequestData rd, out int total)
        {
            var query = @"SELECT        Id, CreationDate, LastModified, IsDeleted, StartTweetId, EndTweetId, CreatedByUserId,
                                IsFinished, UserId, Status, StartTime, FinishTime, TaskDuration, 
                               (SELECT Name FROM user where Id=UserId) as Name,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            AnnotationTaskUserTweet
                               WHERE        (AnnotationTaskId = a.Id) AND (IsDeleted = 0)) AS TotalTweets,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            AnnotationTaskUserTweet AS AnnotationTaskUserTweet_1
                               WHERE        (AnnotationTaskId = a.Id) AND (IsDeleted = 0) AND (Status = 30)) AS DoneTweets
                                FROM            AnnotationTask AS a";

            return GetAll(rd, out total, query, true).ToList();
        }

        public void DeleteAll()
        {
            Execute($"DELETE FROM {TableName};");
        }
    }

    public class AnnotationTask : BaseIntModel
    {
        //public string Name { get; set; }
        public int StartTweetId { get; set; }
        public int EndTweetId { get; set; }
        public int CountTweetId { get; set; }
        public int CreatedByUserId { get; set; }
        //public bool IsFinished { get; set; }
        public int UserId { get; set; }
        public String Name { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? FinishTime { get; set; }
        public int TaskDuration { get; set; }
        public int TotalTweets { get; set; }
        public int DoneTweets { get; set; }
        public int Status { get; set; }


        public int TotalAnnotations { get; set; }
        // public int[] UserIds { get; set; }
    }

    //public class AnnotationTaskUserRepository : BaseIntRepository<AnnotationTaskUser>
    //{
    //    public AnnotationTaskUserRepository()
    //    {
    //        base.Init("AnnotationTaskUser", "UserId,AnnotationTaskId");
    //    }
    //}

    //public class AnnotationTaskUser : BaseIntModel
    //{
    //    public int UserId { get; set; }
    //    public int AnnotationTaskId { get; set; }
    //}


    public class AnnotationTaskUserTweetRepository : BaseIntRepository<AnnotationTaskUserTweet>
    {
        public AnnotationTaskUserTweetRepository()
        {
            base.Init("AnnotationTaskUserTweet", "TaskDuration,LastAction,UserId,AnnotationTaskId,TweetId,Status,StartTime,FinishTime,IsIrrelevant,LevelOfConfidenceId");
        }

        public AnnotationTaskUserTweet GetNext(string userId)
        {
            return GetWhere($"UserId={userId} and (Status={(int)AnnotationTaskUserStatusEnum.InProgress} OR Status={(int)AnnotationTaskUserStatusEnum.New}) ").FirstOrDefault();
        }

        public CustomStatistics getCalucatedValues(AnnotationTaskUserTweet ant)
        {
            var str = " SELECT ( SELECT count(*) FROM AnnotationReasonWord_" + ant.UserId + " where IsDeleted=0 and Id in (SELECT id FROM AnnotationReason_" + ant.UserId + " where AnnotationId in (SELECT Id FROM Annotation_" + ant.UserId + " where IsDeleted=0 and  AnnotationTaskUserTweetId=" + ant.Id + " and AnnotationTaskId=" + ant.AnnotationTaskId + "))) as reasons, " +
                " ( SELECT count(distinct CategoryId) FROM Annotation_" + ant.UserId + " where AnnotationTaskUserTweetId=(select id from annotationtaskusertweet where TweetId=" + ant.TweetId + " and userId=" + ant.UserId + ")) as categories ," +
                "       ( SELECT count(distinct DimensionId) FROM Annotation_" + ant.UserId + " where AnnotationTaskUserTweetId=(select id from annotationtaskusertweet where TweetId=" + ant.TweetId + " and userId=" + ant.UserId + ")) as subcategories, " +
                "       ( SELECT avg  (TaskDuration) FROM annotationtaskusertweet where userId=" + ant.UserId + ") as totalTimeTweeting," +
                " TaskDuration as perTime FROM annotationtaskusertweet where id=" + ant.Id;
            var data = Query<CustomStatistics>(str).ToList();
            if (data.Count > 0)
            {
                return data[0];
            }
            return new CustomStatistics();
        }


        public List<AnnotationTaskUserTweet> GetStatisticsStatus(RequestData rd, out int total)
        {
            string Where = "WHERE atut.IsDeleted=0 ";
            String x = "";
            var userFilter = rd.Filter.FirstOrDefault(x => x.Key.ToLower() == "userid");
            
            if (userFilter != null)
            {
                Where += $" AND UserId = '{userFilter.Value}' ";
            }

            var data = Query<AnnotationTaskUserTweet>("SELECT (select name from user where Id=atut.UserId) as UserName,Status,avg(l.Value) as AvgLevelOfConfidence,avg(TaskDuration) as AvgTaskDuration,count(*) as TotalTasks , " +
                                                      "count(*) * 100.0 / (select count(*) from [AnnotationTaskUserTweet] WHERE IsDeleted=0 ) as percentage" +
                " ,sum(TaskDuration) as TotalTaskDuration   FROM [AnnotationTaskUserTweet] atut left join LevelOfConfidence l on l.Id = atut.LevelOfConfidenceId  " + Where + " GROUP BY Status ").ToList();


            total = data.Count;
            return data;
        }
        public UserStat GetStatisticsStatusUsers(int userId)
        {

            String select = " SELECT COUNT(*)  AS CompletedTasks, " +
 " (COUNT(*) / (SELECT  COUNT(*) FROM  AnnotationTaskUserTweet WHERE IsDeleted = 0 and UserId=" + userId + ")*100) AS CompletedPerc,  " +
 " (SELECT  COUNT(*) FROM  AnnotationTaskUserTweet WHERE IsDeleted = 0 and status in (20, 10) and UserId = " + userId + ")  AS RemainTasks,  " +
 " (SELECT SUM(TaskDuration)/3600 FROM annotationtaskusertweet where userId=" + userId + " and status=30) as TotalTaskDuration,  " +
 " (SELECT (SUM(TaskDuration)/(SELECT  count(*) FROM annotationtaskusertweet where userId=" + userId + " and status=30))/60 FROM annotationtaskusertweet where userId=" + userId + " and status=30) as avgTweetTime,  " +
 " (Select (SUM(xx)/(SELECT  count(*) FROM annotationtaskusertweet where userId=" + userId + " and status=30)) from (SELECT count(distinct CategoryId) as xx  FROM Annotation_" + userId + " where isDeleted=0 group by AnnotationTaskUserTweetId) as  tmp) as TotalCategory,  " +
 " (SELECT (count(DimensionId)/(SELECT  count(*) FROM annotationtaskusertweet where userId=" + userId + " and status=30))  FROM Annotation_" + userId + " where isDeleted=0) as TotalDimension, " +
 " (SELECT (count(*)/(SELECT  count(*) FROM annotationtaskusertweet where userId=" + userId + " and status=30)) FROM AnnotationReasonWord_" + userId + " where IsDeleted=0) as TotalReason " +
 " FROM annotationtaskusertweet where userId=" + userId + " group by userId ";
            var data = Query<UserStat>(select).ToList();
            if (data.Count > 0)
            {
                return data[0];
            }
            return new UserStat();
        }

        public int GetAvgLevelOfConfidence(int userId)
        {
            string Where = $"WHERE atut.IsDeleted=0 AND atut.Status=30 AND UserId = '{userId}' ";
            
            var data = Query<AnnotationTaskUserTweet>("SELECT avg(l.Value) as AvgLevelOfConfidence " +
                "FROM [AnnotationTaskUserTweet] atut left join LevelOfConfidence l on l.Id = atut.LevelOfConfidenceId  " + Where + " GROUP BY Status ").ToList();

            if (data.Count> 0)
            {
                return data[0].AvgLevelOfConfidence;
            }
            return -1;
        }


        public string GetViewQuery()
        {
            return
                @"SELECT        atu.Id, atu.CreationDate, atu.LastModified, atu.IsDeleted, atu.TweetId, atu.UserId, atu.AnnotationTaskId, atu.Status, User.Name AS UserName, Tweet.Text AS TweetText, atu.StartTime, atu.FinishTime, 
                         atu.LevelOfConfidenceId, atu.TaskDuration, atu.IsIrrelevant, LevelOfConfidence.Name AS ConfidenceName,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            Annotation
                               WHERE        (AnnotationTaskUserTweetId = atu.Id) AND (IsDeleted = 0)) AS TotalAnnotations
                            FROM            AnnotationTaskUserTweet AS atu INNER JOIN
                         User ON atu.UserId = User.Id INNER JOIN
                         Tweet ON atu.TweetId = Tweet.Id LEFT OUTER JOIN
                         LevelOfConfidence ON atu.LevelOfConfidenceId = LevelOfConfidence.Id";
        }

        public List<AnnotationTaskUserTweet> GetAllView(RequestData rd, out int total)
        {
            return GetAll(rd, out total, GetViewQuery(), true).ToList();
        }
        public List<AnnotationTaskUserTweet> GetAllView(string where,int size,int page)
        {
            return GetAll(size,page,where,"CreationDate Desc", GetViewQuery(),true).ToList();
        }

        public void DeleteAll()
        {
            Execute($"DELETE FROM {TableName};");
        }
    }
    public class AnnotationTaskUserTweet : BaseIntModel
    {
        public bool? IsIrrelevant { get; set; }
        public int? LevelOfConfidenceId { get; set; }

        public DateTime? StartTime { get; set; }
        public DateTime? FinishTime { get; set; }
        public DateTime? LastAction { get; set; }
        public int TaskDuration { get; set; }
        public int Status { get; set; }

        public AnnotationTaskUserStatusEnum AnnotationStatusEnum => (AnnotationTaskUserStatusEnum)Status;
        public int TweetId { get; set; }
        public int UserId { get; set; }
        public int AnnotationTaskId { get; set; }



        public string ConfidenceName { get; set; }
        public string UserName { get; set; }
        public string TweetText { get; set; }

        public int TotalTasks { get; set; }
        public int TotalTaskDuration { get; set; }
        public double Percentage { get; set; }
        public int AvgTaskDuration { get; set; }
        public int AvgLevelOfConfidence { get; set; }

        public List<AnnotationTaskUserTweet> StatusAgregates { get; set; }

        public List<Annotation> Annotations { get; set; }
        public int? NextAnnotationId { get; set; }

        public int TotalAnnotations { get; set; }
        public int categories { get; set; }
        public int subcategories { get; set; }
        public int reasons { get; set; }
        public int perTime { get; set; }
        public int totalTimeTweeting { get; set; }
    }

    public class CustomStatistics : BaseIntModel
    {
        public int categories { get; set; }
        public int subcategories { get; set; }
        public int reasons { get; set; }
        public int perTime { get; set; }
        public int totalTimeTweeting { get; set; }
        public double Category { get; set; }
        public double subCategory { get; set; }
        public double TotalCategory { get; set; }
        public double TotalDimension { get; set; }
        public double TotalReason { get; set; }

    }

    public class UserStat : BaseIntModel
    {
        public int? LevelOfConfidenceId { get; set; }

        public DateTime? FinishTime { get; set; }
        public DateTime? LastAction { get; set; }
        public int TaskDuration { get; set; }
        public int Status { get; set; }

        public AnnotationTaskUserStatusEnum AnnotationStatusEnum => (AnnotationTaskUserStatusEnum)Status;
        public int TweetId { get; set; }
        public int UserId { get; set; }
        public int AnnotationTaskId { get; set; }



        public string ConfidenceName { get; set; }
        public string UserName { get; set; }
        public string TweetText { get; set; }

        public int AvgLevelOfConfidence { get; set; }
        public int AvgTaskDuration { get; set; }
        public double CompletedPerc { get; set; }
        public int CompletedTasks { get; set; }
        public int TotalTaskDuration { get; set; }
        public int TotalTasks { get; set; }
        public int RemainTasks { get; set; }
        public double TotalCategory { get; set; }
        public double TotalDimension { get; set; }
        public double TotalReason { get; set; }
        public double avgTweetTime { get; set; }
        
        public List<AnnotationTaskUserTweet> StatusAgregates { get; set; }

        public List<Annotation> Annotations { get; set; }
        public int? NextAnnotationId { get; set; }

        public int TotalAnnotations { get; set; }

    }
    public enum AnnotationTaskUserStatusEnum
    {
        New = 10, InProgress = 20, Done = 30
    }
}
