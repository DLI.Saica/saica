﻿using Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Core.Cache;
using Core.Repositories;
using Core.Repositories.Annotations;
using Core.Repositories.Twitter;
using ExcelDataReader;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Globalization;
using NReco.Csv;
namespace API.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class TweetController : BaseController
    {
        public static IConfigurationSection settings;
        private static List<Word> savedWords;
        private static List<Tweet> savedTweets;

        [HttpGet]
        public ActionResult<IEnumerable<Tweet>> GetAll()
        {
            var rd = Rd();
            if (rd.Filter.Any(x => x.Key.ToLower() == "statistics"))
            {
                var ls = P.Tweets.GetStatistics(rd, out int totalTweets).ToList();
                T(totalTweets, rd);
                return ls;
            }
            var countFilter = rd.Filter.FirstOrDefault(x => x.Key.ToLower() == "count");
            if (countFilter != null)
            {
                rd.Filter.Remove(countFilter);
                var count = P.Tweets.GetCount(rd);
                T(count, rd);
                return new List<Tweet>();
            }

            var l = P.Tweets.GetAllView(rd, out var total).ToList();
            var returnVal = 100000;
       
            //foreach (Tweet tw in l)

            //{

            //    try
            //    { 
            //        var annotationsTaskUser1 = Pool.I.AnnotationTaskUserTweets.GetAllView("Status = 30 AND TweetId=" + tw.Id + " AND UserId =" + tw.userId1, returnVal, 1).ToList();
            //        var annotationsTaskUser2 = Pool.I.AnnotationTaskUserTweets.GetAllView("Status = 30 AND TweetId=" + tw.Id + " AND UserId =" + tw.userId2, returnVal, 1).ToList();
            //        var annotationsUser1 = Pool.I.Annotations[tw.userId1].GetAllTweetAnnotationsByUser(tw.Id, tw.userId1).ToList();
            //        var annotationsUser2 = Pool.I.Annotations[tw.userId2].GetAllTweetAnnotationsByUser(tw.Id, tw.userId2).ToList();

            //        var annotationsUserReason1 = Pool.I.AnnotationReasons[tw.userId1].GetAllView(returnVal, 1, tw.userId1).ToList();
            //        var annotationsUserReason2 = Pool.I.AnnotationReasons[tw.userId2].GetAllView(returnVal, 1, tw.userId2).ToList();


            //        var categories = Pool.I.Categorys.GetAll().ToList();
            //        var dimensions = P.Dimensions.GetAll().ToList();
            //        categories.ForEach(x => x.Dimensions = dimensions.Where(z => z.CategoryId == x.Id).OrderBy(z => z.DisplayOrder).ToList());

            //        var intersectedTweets = new List<Tweet>();

            //        foreach (var a in annotationsTaskUser1)
            //        {
            //            if (annotationsTaskUser2.Any(x => x.TweetId == a.TweetId))
            //            {
            //                if (!intersectedTweets.Any(t => t.Id == a.TweetId))
            //                {
            //                    intersectedTweets.Add(new Tweet()
            //                    {
            //                        Id = a.TweetId,
            //                        Text = a.TweetText,
            //                    });
            //                }
            //            }
            //        }
            //        var lst = new List<TweetAgreement2>();

            //        foreach (var t in intersectedTweets)
            //        {
            //            var tweetAnnotation1 = annotationsUser1.Where(x => x.TweetId == t.Id).ToList();
            //            var tweetAnnotation2 = annotationsUser2.Where(x => x.TweetId == t.Id).ToList();

            //            var tweetAnnotationReason1 = annotationsUserReason1.Where(x => x.TweetId == t.Id).ToList();
            //            var tweetAnnotationReason2 = annotationsUserReason2.Where(x => x.TweetId == t.Id).ToList();

            //            var words = Pool.I.TweetWords.GetWhere("TweetId=" + t.Id).ToList();

            //            var ta = new TweetAgreement2
            //            {
            //                Id = t.Id,
            //                TweetId = t.Id,
            //                TweetText = t.Text
            //            };

            //            AgreementCategory(ta, categories, words, tweetAnnotation1, tweetAnnotation2, tweetAnnotationReason1, tweetAnnotationReason2);
            //            tw.Agree1 = ta.CategoryAgreement;
            //            tw.Agree2 = ta.DimensionAgreement;
            //            tw.Agree3 = ta.ReasonAgreement;

            //        }
            //    }
            //    catch (Exception e)
            //    {
            //    }
            //}
                T(total, rd);
             
            return l;
        }

        [HttpGet("{id}/annotations")]
        public ActionResult<TweetAnnotationsData> GetAnnotations(int id)
        {
            var tweet = P.Tweets.Get(id);

            if (tweet == null)
                return NotFound();

            var users = P.Users.GetAll();
            var allCategories = P.Categorys.GetAll();
            var allSubCategories = P.Dimensions.GetAll();
            var tweetAnnotationsData = new TweetAnnotationsData();
            foreach (var user in users)
            {
                if (user.RoleEnum != RoleEnum.Admin)
                {
                    var annotations = P.Annotations[user.Id].GetAllTweetAnnotationsByUser(tweet.Id, user.Id);
                    if (annotations.Count > 0)
                    {
                        foreach (Annotation annotation in annotations)
                        {
                            if (!tweetAnnotationsData.Categories.ContainsKey(annotation.CategoryId.Value.ToString()))
                            {
                                tweetAnnotationsData.Categories[annotation.CategoryId.Value.ToString()] = allCategories.FirstOrDefault((c) => c.Id == annotation.CategoryId);
                            }
                            if (!tweetAnnotationsData.SubCategories.ContainsKey(annotation.DimensionId.Value.ToString()))
                            {
                                tweetAnnotationsData.SubCategories[annotation.DimensionId.Value.ToString()] = allSubCategories.FirstOrDefault((sc) => sc.Id == annotation.DimensionId);
                            }
                        }
                        tweetAnnotationsData.Users[user.Id.ToString()] = user;
                        tweetAnnotationsData.UserAnnotations[user.Id.ToString()] = annotations;
                    }
                }
            }
            return tweetAnnotationsData;
        }
        private void AgreementCategory(TweetAgreement2 ta, List<Category> categories, List<TweetWord> words,
            List<Annotation> tweetAnnotation1, List<Annotation> tweetAnnotation2,
            List<AnnotationReason> tweetAnnotationReason1,
            List<AnnotationReason> tweetAnnotationReason2)
        {
            Matrix matrix = new Matrix();
            foreach (var category in categories)
            {
                var tc1 = tweetAnnotation1.Any(x => x.CategoryId == category.Id) ? 1 : 0;
                var tc2 = tweetAnnotation2.Any(x => x.CategoryId == category.Id) ? 1 : 0;
                FillMatrix(matrix, tc1, tc2);
            }
            var calc = CalculateMetrics(matrix);

            ta.CategoryAgreement = calc.F1Measure;


            Matrix matrixDimension = new Matrix();
            var dimensions = categories.SelectMany(x => x.Dimensions).ToList();
            foreach (var d in dimensions)
            {
                var tc1 = tweetAnnotation1.Any(x => x.DimensionId == d.Id) ? 1 : 0;
                var tc2 = tweetAnnotation2.Any(x => x.DimensionId == d.Id) ? 1 : 0;
                FillMatrix(matrixDimension, tc1, tc2);
            }
            var calcDimension = CalculateMetrics(matrix);
            ta.DimensionAgreement = calcDimension.F1Measure;


            Matrix matrixReason = new Matrix();
            foreach (var w in words)
            {
                foreach (var dimension in dimensions)
                {
                    var tc1 = tweetAnnotationReason1.Any(x => x.DimensionId == dimension.Id && x.StartWordId == w.Id) ? 1 : 0;
                    var tc2 = tweetAnnotationReason2.Any(x => x.DimensionId == dimension.Id && x.StartWordId == w.Id) ? 1 : 0;
                    FillMatrix(matrixReason, tc1, tc2);
                }
            }

            var calcReason = CalculateMetrics(matrixReason);
            ta.ReasonAgreement = calcReason.F1Measure;
        }





        private void FillMatrix(Matrix m, int actual, int predicted)
        {
            if (actual == 1 && predicted == 1)
                m.TruePositive++;
            else if (actual == 1 && predicted == 0)
                m.FalsePositive++;
            else if (actual == 0 && predicted == 1)
                m.FalseNegative++;
            else if (actual == 0 && predicted == 0)
                m.TrueNegative++;
        }

        private MatrixCalculations2 CalculateMetrics(Matrix m)
        {
            var precision = m.TruePositive + m.FalsePositive == 0 ? -1 : m.TruePositive * 1.0 / (m.TruePositive + m.FalsePositive);
            var recall = m.TruePositive + m.FalseNegative == 0 ? -1 : m.TruePositive * 1.0 / (m.TruePositive + m.FalseNegative);
            var f1Measure = precision + recall == 0 ? 0 : (2 * (precision * recall) / (precision + recall));

            return new MatrixCalculations2()
            {
                Recall = Math.Truncate(recall * 100) / 100,
                Precision = Math.Truncate(precision * 100) / 100,
                F1Measure = Math.Truncate(f1Measure * 100) / 100
            };
        }

        [HttpGet("{id}")]
        public ActionResult<Tweet> Get(int id)
        {
            var i = P.Tweets.Get(id);

            if (i == null)
                return NotFound();

            //var words = P.Words.GetAll().ToList();
            var tweetWords = P.TweetWords.GetWhere($"TweetId='{id}'");

            i.WordIds = tweetWords.OrderBy(x => x.Position).Select(z => z.WordId).ToArray();
            var words = P.Words.GetByIds(i.WordIds).ToList();
            i.TweetWords = new List<TweetWord>();

            foreach (var tw in tweetWords)

            {
                
                try
                {
                    var word = words.Single(x => x.Id == tw.WordId);
                    tw.WordName = word.Name;
                }
                catch (Exception e) {
                }
                i.TweetWords.Add(tw);
            }
            return i;
        }

        
        private static void ReadSavedData()
        {
            Console.WriteLine("Reading saved words from database...");
            savedWords = Pool.I.Words.GetAll().ToList();
            Console.WriteLine("Reading saved tweets from database...");
            savedTweets = Pool.I.Tweets.GetAll().ToList();
        }
        [HttpPost]
        public ActionResult Post([FromForm] IFormFile file)
        {
            /*string fileName = $"ExcelFile\\{file.FileName}";
            using (FileStream fileStream = System.IO.File.Create(fileName))
            {
                file.CopyTo(fileStream);
                fileStream.Flush();
            }*/
            var importedTweets = ReadTweetsFromCsvFile(file);
            
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            settings = configuration.GetSection("AppSettings");
            Pool.I.ConnectionString = settings["Connection"];


            /*foreach (var tweet in tweets)
            {
                P.Tweets.Save(tweet);
            }*/
            ReadSavedData();

            importedTweets = RemoveDuplicates(importedTweets);
            if (importedTweets.Count() > 0)
            {

                var newTweets = new List<Tweet>();
                Console.Write("Selecting new tweets only...");
                newTweets = SelectNewTweetsFrom(importedTweets);
                Console.WriteLine($" {newTweets.Count()} new tweets");

                List<Tweet> tweets;
                if (newTweets.Count() > 0)
                {
                    Console.WriteLine("Inserting new tweets into the database...");
                    foreach (var tweetssss in newTweets)
                    {
                        Pool.I.Tweets.Insert(tweetssss);
                    }
                    

                    Console.Write("Fetching new inserted tweets from the database...");
                    tweets = Pool.I.Tweets.GetWhere("TweetId IN (" + string.Join(",", newTweets.Select((t) => $"{t.TweetId}").ToList()) + ")");
                    Console.WriteLine($" {tweets.Count()} new inserted tweets");
                }
                else
                {
                    Console.Write("Fetching tweets from the database...");
                    tweets = Pool.I.Tweets.GetWhere("TweetId IN (" + string.Join(",", importedTweets.Select((t) => $"{t.TweetId}").ToList()) + ")");
                    Console.WriteLine($" {tweets.Count()} tweets");
                }

                Console.WriteLine("Tokenizing new tweets...");
                var tweetTokens = TokenizeTweets(tweets);
                Console.WriteLine("Saving tokens of new tweets into database...");
                foreach (var tweetTokenssss in tweetTokens)
                {
                    Pool.I.TweetWords.Insert(tweetTokenssss);
                }
            }


            return Ok("200");
        }


        private static List<Tweet> ReadTweetsFromCsvFile(IFormFile file)
        {
            var tweets = new List<Tweet>();
            if (file !=null)
            {
                //CSV file template
                //Tweet ID | Text | Account Display Name | User ID | Date | lang
                using (var stream = file.OpenReadStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        var rowNum = 1;
                        var csvReader = new CsvReader(reader, ",");
                        while (csvReader.Read())
                        {
                            if (rowNum > 1)
                            {
                                var tweet = new Tweet()
                                {
                                    CreationDate = DateTime.Now,
                                    LastModified = DateTime.Now,
                                };
                                for (int colIndex = 0; colIndex < csvReader.FieldsCount; colIndex++)
                                {

                                    string cellVal = csvReader[colIndex];

                                    switch (colIndex)
                                    {
                                        case 0:
                                            tweet.TweetId = long.Parse(cellVal.Trim('#'));
                                            break;
                                        case 1:
                                            tweet.Text = cellVal;
                                            break;
                                        case 5:
                                            tweet.AccountDisplayName = cellVal;
                                            break;
                                        case 4:
                                            tweet.UserID = long.Parse(cellVal.Trim('#'));
                                            break;
                                        case 3:
                                            tweet.TweetCreationDate = Convert.ToDateTime(cellVal);
                                            break;
                                        case 6:
                                            tweet.Lang = cellVal;
                                            break;
                                    }
                                }
                                tweets.Add(tweet);
                            }
                            rowNum++;
                        }
                    }
                }
            }
            return tweets;
        }

        private List<Tweet> GetTweetsList(IFormFile file)
        {
            List<Tweet> tweets = new List<Tweet>();
            using (var stream = file.OpenReadStream())
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    while (reader.Read())
                    {
                        tweets.Add(new Tweet()
                        {
                            Text = reader.GetValue(0) == null ? "" : reader.GetValue(0).ToString()
                        });
                    }
                }
                if (tweets != null)
                {
                    tweets = tweets.Where(s => !string.IsNullOrWhiteSpace(s.Text)).Distinct().ToList();
                }
                
                return tweets;
            }

        }


        private static List<Tweet> RemoveDuplicates(List<Tweet> tweets)
        {
            if (tweets != null && tweets.Count > 0)
            {
                List<Tweet> uniqueTweets = new List<Tweet>();
                foreach (Tweet tweet in tweets)
                {
                    if (uniqueTweets.SingleOrDefault((t) => t.TweetId == tweet.TweetId) == null)
                    {
                        uniqueTweets.Add(tweet);
                    }
                }
                return uniqueTweets;
            }
            return tweets;
        }

        private static List<Tweet> SelectNewTweetsFrom(List<Tweet> tweets)
        {
            if (tweets != null && tweets.Count > 0)
            {
                tweets = tweets.Where((t) => savedTweets.SingleOrDefault((tt) => t.TweetId == tt.TweetId) == null).ToList();
            }
            return tweets;
        }

        private static List<TweetWord> TokenizeTweets(List<Tweet> tweets)
        {
            var tweetWords = new List<TweetWord>();
            foreach (var tweet in tweets)
            {
                tweetWords.AddRange(TokenizeTweet(tweet));
            }
            return tweetWords;
        }

        private static List<TweetWord> TokenizeTweet(Tweet tweet)
        {
            var tweetWords = new List<TweetWord>();
            if (tweet != null)
            {
                var textTokens = TokenizeTweetText(tweet.Text);
                int i = 1;
                foreach (var tt in textTokens)
                {
                    if (!string.IsNullOrEmpty(tt))
                    {
                        var word = FindWordOrCreateIt(tt);

                        tweetWords.Add(new TweetWord()
                        {
                            CreationDate = DateTime.Now,
                            LastModified = DateTime.Now,
                            TweetId = tweet.Id,
                            WordId = word.Id,
                            Position = i++
                        });
                    }
                }
            }
            return tweetWords;
        }

        private static List<string> TokenizeTweetText(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                /*var textWithoutDiacritics = RemoveDiacritics(text);*/
                var textWithoutDiacritics =(text);

                //var punctuation = textWithoutDiacritics.Where(Char.IsPunctuation).Distinct().ToArray();
                return text
                    /*.CleanTweetUrLs()
                    .CleanUsername()
                    .Replace("@", " ")
                    .Replace(".", " ")
                    .Replace("#", " ")
                    .Replace("?", " ")
                    .Replace(",", " ")
                    .Replace("،", " ")
                    .Replace(":", " ")
                    .Replace("!", " ")
                    .Replace("_", " ")
                    .Replace("\n", "")
                    .Replace("\r", "")
                    .Replace("\t", "")
                    .Replace("  ", " ")
                    .RemoveEmojies()
                    .RemoveNumbers()*/
                    .Trim()
                    .Split(" ").ToList();
            }
            return new List<string>();
        }

        private static string RemoveDiacritics(string text)
        {

            if (text != null)
            {


                var normalizedString = text.Normalize(NormalizationForm.FormD);
                var stringBuilder = new StringBuilder();

                foreach (var c in normalizedString)
                {
                    var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                    if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                    {
                        stringBuilder.Append(c);
                    }
                }

                return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
            }
            return "";
        }

        private static Word FindWordOrCreateIt(string s)
        {
            Word word = savedWords.SingleOrDefault(w => w.Name == s);

            if (word == null)
            {
                word = new Word() { Name = s };
                Pool.I.Words.Save(word);
                savedWords.Add(word);
            }

            return word;
        }


        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id)
        {
            if (!IsAdmin) return Unauthorized();
            var i = P.Tweets.Get(id);
            if (i == null)
                return NotFound();
            P.Tweets.Save(i);
            return Ok(i);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!IsAdmin) return Unauthorized();
            var i = P.Tweets.Get(id);
            if (i == null)
                return NotFound();

            P.Tweets.Delete(id);
            return Ok(i);
        }


        public class TweetAnnotationsData
        {
            public Dictionary<string, List<Annotation>> UserAnnotations { get; }
            public Dictionary<string, Category> Categories { get; }
            public Dictionary<string, Dimension> SubCategories { get; }
            public Dictionary<string, User> Users { get; }

            public TweetAnnotationsData()
            {
                UserAnnotations = new Dictionary<string, List<Annotation>>();
                Categories = new Dictionary<string, Category>();
                SubCategories = new Dictionary<string, Dimension>();
                Users = new Dictionary<string, User>();
            }
        }

    }

}
