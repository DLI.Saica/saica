﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Services;
using Core.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class UsersController : BaseController
    {
        [HttpGet]
        public ActionResult<IEnumerable<User>> GetAll()
        {
            var rd = Rd();
            var countFilter = rd.Filter.FirstOrDefault(x => x.Key.ToLower() == "count");
            if (countFilter != null)
            {
                rd.Filter.Remove(countFilter);
                var count = P.Users.GetCount(rd);
                T(count, rd);
                return new List<User>();
            }
            String order="", sort = "";
            List<String> ord = new List<String>(new string[] {"totalTasks",
                  "remainTasks",
                  "percentage",
                  "totalTaskDuration",
                  "avgTweetTime",
                  "totalCategory",
                  "totalDimension",
                  "totalReason" });


            if (ord.Contains(rd.Order))
            {
                order = rd.Order;
                sort = rd.Sort;
                rd.Order = "name";
                rd.Sort = "asc";
            }
            rd.Filter.Add(new FilterData("Role", "eq","30"));
            var l = P.Users.GetAll(rd, out var total).ToList();
            foreach(var u in l)
            {
                if(u.RoleEnum != RoleEnum.Admin)
                {
                    u.AvgLevelOfConfidence = P.AnnotationTaskUserTweets.GetAvgLevelOfConfidence(u.Id);
                    u.statistics= P.AnnotationTaskUserTweets.GetStatisticsStatusUsers(u.Id);
                    u.TotalTasks = u.statistics.TotalTasks;
                    u.FinishedTasks = u.statistics.CompletedTasks;
                    u.Percentage = u.statistics.CompletedPerc;
                    u.RemainTasks = u.statistics.RemainTasks;
                    u.TotalTaskDuration = u.statistics.TotalTaskDuration;
                    u.TotalCategory = u.statistics.TotalCategory;
                    u.TotalDimension = u.statistics.TotalDimension;
                    u.TotalReason = u.statistics.TotalReason;
                    u.avgTweetTime = u.statistics.avgTweetTime;      
                }
                else
                {
                    u.AvgLevelOfConfidence = -1;
                }
            }
            if (sort == "asc")
            {
                if (order == "remainTasks") l.Sort((x, y) => x.RemainTasks.CompareTo(y.RemainTasks));
                if (order == "percentage") l.Sort((x, y) => x.Percentage.CompareTo(y.Percentage));
                if (order == "totalTaskDuration") l.Sort((x, y) => x.TotalTaskDuration.CompareTo(y.TotalTaskDuration));
                if (order == "avgTweetTime") l.Sort((x, y) => x.avgTweetTime.CompareTo(y.avgTweetTime));
                if (order == "totalCategory") l.Sort((x, y) => x.TotalCategory.CompareTo(y.TotalCategory));
                if (order == "totalDimension") l.Sort((x, y) => x.TotalDimension.CompareTo(y.TotalDimension));
                if (order == "totalReason") l.Sort((x, y) => x.TotalReason.CompareTo(y.TotalReason));
            }
            else
            {
                if (order == "remainTasks") l.Sort((x, y) => y.RemainTasks.CompareTo(x.RemainTasks));
                if (order == "percentage") l.Sort((x, y) => y.Percentage.CompareTo(x.Percentage));
                if (order == "totalTaskDuration") l.Sort((x, y) => y.TotalTaskDuration.CompareTo(x.TotalTaskDuration));
                if (order == "avgTweetTime") l.Sort((x, y) => y.avgTweetTime.CompareTo(x.avgTweetTime));
                if (order == "totalCategory") l.Sort((x, y) => y.TotalCategory.CompareTo(x.TotalCategory));
                if (order == "totalDimension") l.Sort((x, y) => y.TotalDimension.CompareTo(x.TotalDimension));
                if (order == "totalReason") l.Sort((x, y) => y.TotalReason.CompareTo(x.TotalReason));
            }
            T(total, rd);
            return l;
        }

        [HttpGet("{id}")]
        public ActionResult<User> Get(int id)
        {
            var i = P.Users.Get(id);
            if (i == null)
                return NotFound();
            return i;
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Post([FromBody] User value)
        {
            if (!IsAdmin) return Unauthorized();
            value.SetPassword(value.Password);
            var old = P.Users.GetByUsername(value.Username);
            if (old != null)
            {
                return NotFound("Username already exists");
            }
            P.Users.Save(value);
            P.Users.CreateNewUserTables(value);
            
            P.InitAnnotationRepositorys();
            return Ok(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] User value)
        {
            if (!IsAdmin) return Unauthorized();
            var i = P.Users.Get(id);
            if (i == null)
                return NotFound();
            if (i.Username != "adminA")
            {
                var old = P.Users.GetByUsername(value.Username);
                if (old != null && old.Id != value.Id)
                {
                    return NotFound("Username already exists");
                }

                i.Name = value.Name;
                i.IsActive = value.IsActive;
                i.Username = value.Username;
                i.Role = value.Role;


                if (!string.IsNullOrEmpty(value.Password))
                    i.SetPassword(value.Password);
                P.Users.Save(i);
            }

            return Ok(value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!IsAdmin) return Unauthorized();
            var i = P.Users.Get(id);
            if (i == null)
                return NotFound();

            if (i.Username == "admin")
                return Error("cannot delete admin record");

            var userTasks = P.AnnotationTasks.GetWhere($"UserId={id}").ToList();
            if (!userTasks.Any())
            {
                P.Users.Delete(id);
            }
            else
            {
                return Error("cannot delete admin record");
            }


            return Ok(i);
        }
    }
}