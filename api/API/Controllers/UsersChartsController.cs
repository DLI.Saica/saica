﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Services;
using Core.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class UsersChartsController : BaseController
    {
        [HttpGet]
        public ActionResult<IEnumerable<Object>> GetAll()
        {
            var rd = Rd();
            rd.Cache = true;
            List<MasterLineChart> list = new List<MasterLineChart>();
            int typeY = rd.Ids[rd.Ids.Count - 1];
            rd.Ids.RemoveAt(rd.Ids.Count - 1);
            
            if (rd.Ids.Count > 0) 
            { 
                foreach (int userId in rd.Ids)
                {
                    List<LineChart> l = new List<LineChart>();
                    if (typeY == 1)
                    {
                        l = P.Users.GetByQueryTweets(userId).ToList();
                    }
                    if (typeY == 2)
                    {
                        l = P.Users.GetByQueryConfidence(userId).ToList();
                    }
                    if (typeY == 3)
                    {
                        l = P.Users.GetByQueryTime(userId).ToList();
                    }
                    
                    MasterLineChart master = new MasterLineChart();
                    master.Id = userId;
                    master.name = l.Count > 0 ? l[0].name : "";
                    master.howInLegend = true;
                    master.type = "spline";
                    master.dataPoints = l;
                    list.Add(master);
                }

                if (typeY == 4)
                {
                    String ids = "";
                    foreach (int userId in rd.Ids)
                    {
                        ids = userId + ",";
                    }
                    ids = ids.Substring(0, ids.Length - 1);
                    var i = P.Dimensions.GetByWordCloudUsers(ids);
                    if (i == null)
                        return NotFound();
                    T(i.Count, rd);
                    return i;
                }
            }
            T(list.Count, rd);
            return list;
        }
    }
}