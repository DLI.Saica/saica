﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Cache;
using Core.Repositories;
using Core.Repositories.Annotations;
using Core.Repositories.Twitter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AnnotationTaskUserTweetController : BaseController
    {
        [HttpGet]
        public ActionResult<IEnumerable<AnnotationTaskUserTweet>> GetAll()
        {
            var rd = Rd();       
            if (User.IsInRole(((int)RoleEnum.Annotator).ToString()))
            {
                rd.Filter.Add(new FilterData("UserId", "eq", User.Identity.Name));
            }

            if (rd.Filter.Any(x => x.Key.ToLower() == "statistics-status"))
            {
                var ls = P.AnnotationTaskUserTweets.GetStatisticsStatus(rd, out int t).ToList();
                T(t, rd);
                return ls;
            }
            var countFilter = rd.Filter.FirstOrDefault(x => x.Key.ToLower() == "count");
            if (countFilter != null)
            {
                rd.Filter.Remove(countFilter);
                var count = P.AnnotationTaskUserTweets.GetCount(rd);
                T(count, rd);
                return new List<AnnotationTaskUserTweet>();
            }

            List<AnnotationTaskUserTweet> l = P.AnnotationTaskUserTweets.GetAllView(rd, out var total).ToList();
            foreach (AnnotationTaskUserTweet ant in l)
            {
                if(ant.Status== (int)AnnotationTaskUserStatusEnum.Done) {
                    CustomStatistics yag = P.AnnotationTaskUserTweets.getCalucatedValues(ant);
                    ant.categories = yag.categories;
                    ant.subcategories = yag.subcategories;
                    ant.reasons = yag.reasons;
                    ant.perTime = yag.perTime;
                    ant.totalTimeTweeting = yag.totalTimeTweeting;
                }
            }
            T(total, rd);
            
            return l;
        }

        [HttpGet("{id}")]
        public ActionResult<AnnotationTaskUserTweet> Get(int id)
        {
            var i = P.AnnotationTaskUserTweets.Get(id);
            if (i == null)
                return NotFound();

            var userId = i.UserId;
            i.UserName = P.Users.Get(userId)?.Name;
            
            i.Annotations = P.Annotations[userId].GetWhere("AnnotationTaskUserTweetId=" + id);
            
            var reasons = P.AnnotationReasons[userId].GetByIds(i.Annotations.Select(x => x.Id).ToArray(), "AnnotationId").ToList();

            var words = P.AnnotationReasonWords[userId].GetByIds(reasons.Select(x => x.Id).ToArray(), "AnnotationReasonId");

            reasons.ForEach(x =>
            {
                x.AnnotationReasonWords = words.Where(z => z.AnnotationReasonId == x.Id).ToList();
            });

            i.Annotations.ForEach(x =>
            {
                x.AnnotationReasons = reasons.Where(z => z.AnnotationId == x.Id).ToList();
            });

            /**/
            return i;
        }     


        //Finish Task
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] AnnotationTaskUserTweet value)
        {
            var i = P.AnnotationTaskUserTweets.Get(id);
            if (i == null)
                return NotFound();
            if (!value.LevelOfConfidenceId.HasValue && value.LevelOfConfidenceId<=0)
                return Error("LevelOfConfidenceId is not provided");

            if (i.Status == (int)AnnotationTaskUserStatusEnum.InProgress) // && User.Identity.Name == i.UserId.ToString()
            {
                i.Status = (int)AnnotationTaskUserStatusEnum.Done;
                i.FinishTime = DateTime.Now;
                i.LastAction = i.FinishTime;
                //to_do

                i.TaskDuration = (int)(i.StartTime.Value.Subtract(DateTime.Now)).TotalSeconds * -1;
                i.IsIrrelevant = value.IsIrrelevant;
                i.LevelOfConfidenceId = value.LevelOfConfidenceId;
                P.AnnotationTaskUserTweets.Save(i);
                value.NextAnnotationId = P.AnnotationTaskUserTweets.GetNext(User.Identity.Name)?.Id ?? 0;
            }
            else
            {
               return Error("cannot edit Done Task");
            }

            return Ok(value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var i = P.AnnotationTaskUserTweets.Get(id);
            if (i == null)
                return NotFound();
            int tweetId = i.TweetId;

            var userId = i.UserId;
            i.UserName = P.Users.Get(userId)?.Name;
            i.Annotations = P.Annotations[userId].GetWhere("AnnotationTaskUserTweetId=" + id);
            var reasons = P.AnnotationReasons[userId].GetByIds(i.Annotations.Select(x => x.Id).ToArray(), "AnnotationId").ToList();
            var words = P.AnnotationReasonWords[userId].GetByIds(reasons.Select(x => x.Id).ToArray(), "AnnotationReasonId");

            reasons.ForEach(x =>
            {
                x.AnnotationReasonWords = words.Where(z => z.AnnotationReasonId == x.Id).ToList();
            });

            i.Annotations.ForEach(x =>
            {
                x.AnnotationReasons = reasons.Where(z => z.AnnotationId == x.Id).ToList();
            });
            if (User.Identity.Name == i.UserId.ToString() && i.Status != (int)AnnotationTaskUserStatusEnum.Done)
            {
                i.StartTime = DateTime.Now;
                i.TaskDuration = 0;
                i.Status = (int)AnnotationTaskUserStatusEnum.InProgress;

                foreach (Annotation ant in i.Annotations)
                {
                    foreach (AnnotationReason antRes in ant.AnnotationReasons)
                    {
                        foreach (AnnotationReasonWord antresWord in antRes.AnnotationReasonWords)
                        {
                            P.AnnotationReasonWords[userId].Delete(antresWord.Id);
                        }
                        P.AnnotationReasons[userId].Delete(antRes.Id);
                    }
                    P.Annotations[userId].Delete(ant.Id);
                }

                var annotationRobot = P.Annotations[1].GetWhere("tweetId=" + tweetId);
                if (annotationRobot.Count > 0) { 
                    var reasonsRobot = P.AnnotationReasons[1].GetByIds(annotationRobot.Select(x => x.Id).ToArray(), "AnnotationId").ToList();
                    var wordsRobot = P.AnnotationReasonWords[1].GetByIds(reasonsRobot.Select(x => x.Id).ToArray(), "AnnotationReasonId");
                    reasonsRobot.ForEach(x =>
                    {
                        x.AnnotationReasonWords = wordsRobot.Where(z => z.AnnotationReasonId == x.Id).ToList();
                    });

                    annotationRobot.ForEach(x =>
                    {
                        x.AnnotationReasons = reasonsRobot.Where(z => z.AnnotationId == x.Id).ToList();
                    });
                
                    foreach (Annotation ant in annotationRobot)
                    {
                        Annotation annotation = new Annotation();
                        annotation.IsDeleted = false;
                        annotation.AnnotationTaskId = i.AnnotationTaskId;
                        annotation.AnnotationTaskUserTweetId = i.Id;
                        annotation.DimensionId = ant.DimensionId;
                        annotation.CategoryId = ant.CategoryId;
                        annotation.CreationDate = DateTime.Now;
                        annotation.LastModified = DateTime.Now;
                        int antId = P.Annotations[userId].Save(annotation);
                        foreach (AnnotationReason antReason in ant.AnnotationReasons)
                        {
                            AnnotationReason antR = new AnnotationReason();
                            antR.AnnotationId = antId;
                            antR.IsDeleted = false;
                            antR.CategoryId = antReason.CategoryId;
                            antR.DimensionId = antReason.DimensionId;
                            antR.CreationDate = DateTime.Now;
                            antR.EndWordId = antReason.EndWordId;
                            antR.LastModified = DateTime.Now;
                            antR.StartWordId = antReason.StartWordId;
                            antR.StartWordPosition = antReason.StartWordPosition;
                            antR.EndWordPosition = antReason.EndWordPosition;
                            int antWordId = P.AnnotationReasons[userId].Save(antR);
                            foreach (AnnotationReasonWord antReasonWord in antReason.AnnotationReasonWords)
                            {
                                AnnotationReasonWord antRW = new AnnotationReasonWord();
                                antRW.AnnotationReasonId = antWordId;
                                antRW.CreationDate = DateTime.Now;
                                antRW.TweetWordId = antReasonWord.TweetWordId;
                                antRW.IsDeleted = false;
                                antRW.LastModified = DateTime.Now;
                                P.AnnotationReasonWords[userId].Save(antRW);
                            }
                        }
                    }
                }
                if (!i.StartTime.HasValue)
                {
                    i.StartTime = DateTime.Now;
                    i.Status = (int)AnnotationTaskUserStatusEnum.InProgress;
                }

                if (!i.StartTime.HasValue)
                {
                    i.StartTime = DateTime.Now;
                    i.TaskDuration = 0;
                    i.Status = (int)AnnotationTaskUserStatusEnum.InProgress;
                }
                i.LastAction = DateTime.Now;
                P.AnnotationTaskUserTweets.Save(i);
            }
            return Ok(i);
        }
    }
}
