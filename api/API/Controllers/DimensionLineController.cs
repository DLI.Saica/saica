﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Cache;
using Core.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class DimensionLineController : BaseController
    {
        [HttpGet]
        public ActionResult<IEnumerable<MasterLineChart>> GetAll()
        {
            var rd = Rd();
            rd.Cache = true;
            List < MasterLineChart > list =new List<MasterLineChart>();
            int typeY = rd.Ids[rd.Ids.Count-1];
            rd.Ids.RemoveAt(rd.Ids.Count - 1);
            if (typeY == 1)
            {
                foreach (int dimId in rd.Ids)
                {
                    List<LineChart> l = P.Dimensions.GetByQueryLineDay(dimId+"").ToList();
                    MasterLineChart master = new MasterLineChart();

                    master.Id = dimId;
                    master.name = l.Count > 0 ? l[0].name : "";
                    master.howInLegend = true;
                    master.type = "spline";
                    master.dataPoints = l;
                    list.Add(master);

                }
            }
            if (typeY == 2)
            {
                foreach (int dimId in rd.Ids)
                {
                    List<LineChart> l = P.Dimensions.GetByQueryLineWeek(dimId + "").ToList();
                    MasterLineChart master = new MasterLineChart();

                    master.Id = dimId;
                    master.name = l.Count > 0 ? l[0].name : "";
                    master.howInLegend = true;
                    master.type = "spline";
                    master.dataPoints = l;
                    list.Add(master);
                }
            }
            if (typeY == 3)
            {
                foreach (int dimId in rd.Ids)
                {
                    List<LineChart> l = P.Dimensions.GetByQueryLineMonth(dimId+"").ToList();
                    MasterLineChart master = new MasterLineChart();

                    master.Id = dimId;
                    master.name = l.Count > 0 ? l[0].name : "";
                    master.howInLegend = true;
                    master.type = "spline";
                    master.dataPoints = l;
                    list.Add(master);
                }
            }
            T(list.Count, rd);
            return list;
        }
    }
}
