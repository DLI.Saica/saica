﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Cache;
using Core.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class SubCategoryCloudController : BaseController
    {
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<Dimension>> Get(int id)
        {
            var rd = Rd();
            rd.Cache = true;
            var l = P.Dimensions.GetAll(rd, out var total).ToList();
            T(total, rd);
            return l;
        }

        [HttpGet]
        public ActionResult<IEnumerable<SubCategory>> GetAll()
        {
            var rd = Rd();
            var userFilter = rd.Filter.FirstOrDefault(x => x.Key.ToLower() == "id");
            var i = P.Dimensions.GetByWordCloud(userFilter.Value);
            if (i == null)
                return NotFound();
            T(i.Count, rd);
            return i;
        }

        [HttpPost]
        public Dimension Post([FromBody] Dimension value)
        {
            if (!IsAdmin) return null;
            P.Dimensions.Save(value);
            return value;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Dimension value)
        {
            var i = P.Dimensions.Get(id);
            if (i == null)
                return NotFound();
            P.Dimensions.Save(value);
            return Ok(value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!IsAdmin) return Unauthorized();
            var i = P.Dimensions.Get(id);
            if (i == null)
                return NotFound();

            var users = P.Users.GetAll();
            var exists = false;
            foreach (var user in users)
            {
                var annotations = P.Annotations[user.Id].GetWhere($"DimensionId={id}").ToList();
                if (annotations.Any())
                {
                    exists = true;
                    break;
                }
            }
            if (!exists)
            {
                P.Dimensions.Delete(id);
            }
            else
            {
                return Error("cannot delete dimension");
            }
            return Ok(i);
        }
    }
}
