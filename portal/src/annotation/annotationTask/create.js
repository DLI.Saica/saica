import React from 'react';

import { required, Create, SimpleForm, NumberInput, TextInput, BooleanField, REDUX_FORM_NAME, ReferenceInput, SelectInput,AutocompleteInput } from 'react-admin';
import { change } from 'redux-form'



const UserCreate = (props) => (
    
    <Create {...props} undoable={false}>
         
        <SimpleForm  redirect="show" >
        
            {/* <TextInput source="name" validate={required()} /> */}
            <ReferenceInput source="userId" reference="users" label="Users" validate={required()}    filter={{ role: 30 }} >
                <SelectInput optionText="name" />
            </ReferenceInput>
           
            <NumberInput id="startTweetId"  source="startTweetId" label="Start Tweet" validate={required()} />
            <NumberInput id="countTweetId" 
            onChange={value => document.getElementById('endTweetId').value= parseFloat(document.getElementById('countTweetId').value)+parseFloat(document.getElementById('startTweetId').value)-1}
            onBlur={value => document.getElementById('endTweetId').value= parseFloat(document.getElementById('countTweetId').value)+parseFloat(document.getElementById('startTweetId').value)-1}
            onLoad={value => document.getElementById('endTweetId').value= parseFloat(document.getElementById('countTweetId').value)+parseFloat(document.getElementById('startTweetId').value)-1}
            source="countTweetId" label="Count Tweet" validate={required()} />
            <NumberInput disabled='disabled' id="endTweetId"/>
        </SimpleForm>
        
    </Create>

);



export default UserCreate;

