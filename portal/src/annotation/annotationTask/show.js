import React from 'react';
import {
  Datagrid,
  DateField,
  ShowButton,
  ReferenceManyField,ReferenceField,
  RichTextField,
  Show,List,CardActions,
  Tab,translate,
  TabbedShowLayout,
  TextField,Pagination,
  downloadCSV} from 'react-admin';

import compose from 'recompose/compose';


const Status = props => {
  console.log(props.record);
  if (props.record.status == 10) {
    return <span className="mh-status mh-status-gray" >New</span>
}
else if (props.record.status == 20) {
    return <span className="mh-status mh-status-yellow" >Progress</span>
}
else if (props.record.status == 30) {
    return <span className="mh-status mh-status-green" >Done</span>
}
    return <span>_</span>
};
const PostPanel = ({ id, record, resource }) => (
  <div dangerouslySetInnerHTML={{ __html: record.tweetText }} />
);
const NoneActions = props => (
  <CardActions />
);

const TaskShow = props => (
  <Show {...props}>
    <TabbedShowLayout>
      <Tab label="resources.annotationTask.tweets">
        <ReferenceManyField
          perPage={25}
          pagination={<Pagination rowsPerPageOptions={[25, 50, 100]}/>}
          addLabel={false}
          reference="annotationTaskUserTweet"
          target="annotationTaskId"
          sort={{ field: 'tweetId', order: 'ASC' }}
        >
          <Datagrid expand={<PostPanel />} >
            <TextField source="tweetId" label="resources.general.tweet"/>
            <TextField source="reasons" label="resources.words.name"/>
            <TextField source="categories" label="resources.reset.categories"/>
            <TextField source="subcategories" label="resources.dimension.name"/>
            <TextField source="perTime" label="resources.users.timeSpentSecond" />
            <TextField source="totalTimeTweeting" label="resources.users.timeSpentAvg"/>
              <TextField source="confidenceName" label="resources.annotations.confidence" />
              {/* <TextField source="totalAnnotations" label="resources.annotations.totalAnnotations" />             */}
              <Status label="Status" source="status" label="resources.annotations.status" options={{ width: 30 }}  translate={translate} />       
              <ShowButton />
          </Datagrid>
        </ReferenceManyField>
        {/* <AddCommentButton /> */}
      </Tab>
    </TabbedShowLayout>
  </Show>
);

const enhance = compose(
  translate  );

export default enhance(TaskShow);
