import React from 'react';
import { AppBar, UserMenu, MenuItemLink, withTranslate } from 'react-admin';
import Typography from '@material-ui/core/Typography';
import SettingsIcon from '@material-ui/icons/Settings';
import { withStyles } from '@material-ui/core/styles';
import ResetMenuItem from './ResetMenuItem';
import './menu.css';

import Logo from './Logo';

const styles = {
    title: {
        flex: 1,
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
    },
    spacer: {
        flex: 1,
    },
};

const CustomUserMenu = (props) => {
    const { translate } = props;
    return <UserMenu {...props}>
        <MenuItemLink
            to="/configuration"
            primaryText={translate('resources.general.configuration')}
            leftIcon={<SettingsIcon />}
        />
        <ResetMenuItem {...props} />
    </UserMenu>
};

const CustomAppBar = (props) => {
    const { translate, classes } = props;
    return <AppBar {...props}
        userMenu={<CustomUserMenu {...props} />}>
        <div style={{ width: '40%' }}>
            <Typography
                variant="title"
                color="inherit"
                className={classes.title}
            >
            <a style={{ color: 'white', textDecoration: 'none', fontSize: '16px' }} href='#/'>
                    {translate('resources.general.appname')}</a>
            </Typography>
        </div>
        <div style={{ width: '30%', textAlign: 'center' }}><Logo /></div>
        <div style={{ width: '30%', textAlign: 'end' }}>{translate('resources.general.welcome')}  {JSON.parse(localStorage.getItem('user')).name}</div>
        <div style={{zIndex:'9', position:'absolute', visibility:localStorage.getItem('role')=='admin'?'visible':'hidden'}} ><br/><br/><br/><br/><br/><br/>
        <a className={"extern"}  href="#/statistics/overview">{translate('resources.reporting.name')}</a>
        <a className={"extern"}  href="#/annotationtask">{translate('resources.annotations.name')}</a>
        <a className={"extern"}  href="#/tweet">{translate('resources.twitter.name')}</a>
        <a className={"extern"}  href="#/agreement">{translate('resources.agreement.name')}</a>
        <a className={"extern"}  href="#/category">{translate('resources.category.name')}</a>
        <a className={"extern"}  href="#/users">{translate('resources.users.name')}</a>
        </div>
    </AppBar>;
};

export default withStyles(styles)(withTranslate(CustomAppBar));
