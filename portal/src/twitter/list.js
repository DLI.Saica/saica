import React from 'react';
import TweetText from './TweetText';

import { NumberInput,Create, SimpleForm, List, Datagrid, TextField, Filter, TextInput, BooleanField, ReferenceInput, SelectInput,ReferenceField , Pagination } from 'react-admin';
const UserFilter = props => (
    <Filter {...props}>
        <TextInput label="Search" source="text"  label='resources.general.search' />
        <NumberInput label="Assigned To Users" source="assignedInTasks" alwaysOn />
    </Filter>
);


const PostPagination = props => <Pagination rowsPerPageOptions={[25, 50, 100]} {...props} />
const UserList = ({ translate, ...props }) => (
    <List perPage={25} {...props} filters={<UserFilter />} pagination={<PostPagination />}>
        <Datagrid rowClick="show">
        <TextField source="id" label='#' />
        <TextField source="assignedInTasks" label='Member Name1' />
        <TextField source="assignedInTasks2" label='Member Name2' />
        <TextField source="assignedInTasks3" label='Member Name3' />
        <TextField source="dimention" label='Dimension Used' />
        <TweetText source="text" label='text' />
        </Datagrid>
    </List>
);
export default UserList;

