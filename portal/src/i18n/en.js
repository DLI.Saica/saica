import englishMessages from 'ra-language-english';

export default {
    ...englishMessages,
    resources: {
        general: {
            appname: 'Monitoring Hate Speech Using Big Data',
            configuration: 'Language',
            language: 'Language',
            active: 'Active',
            search: 'Search',
            displayOrder: 'Order',
            back: 'back',
            next: 'next',
            done: 'Done',
            name: 'name',
            task: 'Task',
            reset: 'Reset',
            welcome: 'Welcome ',
            assigned:'Assigned User',
            cancel: 'Cancel'
        },
        reset:{
            title: 'Reset system data',
            description: "The data will be removed permenantly. You can't undo this action.",
            tasks_and_annotations: "Tasks and annotations",
            categories: "Categories",
            tweets: "Tweets",
            be_careful: "Be careful"
        },
        agreement: {
            name: 'Agreement'
        },
        annotations: {
            tweetNumber: ' Tweet Number ',
            text: 'Text',
            ChooseAWord:'Choose A Word',
            ChooseASentence:'Choose A Sentence',
            removeDimension: 'Remove Answer',
            isIrrelevent: 'isIrrelevent',
            confidence: 'Confidence',
            totalAnnotations: 'Total Annotations',
            name: 'Tasks',
            taskDuration: "Task Duration",
            monitor: 'Task Details',
            annotationtask: "Tasks",
            status: 'Status',
            startTime: 'Start Time',
            finishTime: 'Finish Time',
            taskDone: 'Complete Task'
        },
        annotationTask: {
            nameItem: 'Task',
            tweets: 'Tweets',
            name: 'Name',
            user: 'Annotator',
            totalTweets: 'Total Tweets',
            doneTweets: 'Done Tweets',
            status: 'Status',
            startTweetId: 'Start Tweet',
            endTweetId: 'End Tweet',
        },
        words: {
            name: 'Words'
        }
        ,
        levelOfConfidence: {
            name: 'Confidence'
        },
        twitter: {
            tweet: 'Tweet',
            name: 'Twitter'
        }
        ,
        category: {
            name: 'Categories',
            category: 'Category',
            color: 'Color'
        }
        ,
        dimension: {
            nameAr: 'Sub-category ar',
            nameEn: 'Sub-category en',
            choose: 'Choose ',
            name: 'Sub-categories',
            nameitem: 'Sub-category',
            add: 'Add sub-category',
            chooselevel: 'Confidence'
        }
        ,
        users: {
            statistics: 'Statistics',
            user: 'User',
            name: 'Users',
            username: 'Name',
            role: 'Role',
            annotations: 'Annotations',
            avgLevelOfConfidence: 'Avg Per Confidence',
            allTweets: 'All Tweet',
            completedTasks: 'Completed Tasks',
            pendingTasks: 'Pending Tasks',
            percentCompleted: 'Percent Completed',
            timeSpent: 'Time Spent',
            timeSpentSecond: 'Seconds Spent',
            timeSpentAvg: 'Avarge Seconds Spent',
            chosenWords: 'Chosen Words',
            precision: 'Precision',
            avgTweetTime: 'Avg Time/Tweet (Min)',
            totalCategory:'Avg Category',
            totalDimension:'Avg Dimension',
            totalReason:'Avg Reasons',
            totalTaskDuration:'Total Task Duration (Hour)'
        },
        statistics: {
            allTweets: 'All Tweet',
            tweetDone: 'Tweet Done',
            tweetRemaining: 'Tweet Remaining',
            donePercentage: 'Done Percentage',
            workMinutes: 'Work Minutes',
            averagePerTweet: 'Avg Per Tweet',
            avgLevelOfConfidence: 'Avg Per Confidence'
        }
        ,
        reporting: {
            name: 'Reporting'
        },
        status: {
            new: 'New', progress: 'Progress', done: 'Done'
        }
    }
};