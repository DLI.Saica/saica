import React from 'react';
import Typography from '@material-ui/core/Typography';
import { unparse as convertToCSV } from 'papaparse/papaparse.min';
import { Create, SimpleForm,List, Datagrid, TextField, Filter,TextInput,BooleanField,downloadCSV , Pagination } from 'react-admin';
import { TableFooter } from '@material-ui/core';
import jsonExport from 'jsonexport/dist';

const UserFilter = props => (
    <Filter {...props}>
        <TextInput label="Search" source="name" alwaysOn  label='resources.general.search'/>
    </Filter>
);


const exporter = data => {
    const csv = convertToCSV({
        data,
        fields: ['name', 'roleName', 'avgLevelOfConfidence', 'totalTasks','finishedTasks',
        'remainTasks','percentage'],
    });
    downloadCSV(csv, 'users'); // download as 'somelist.csv` file
};
/*const exporter = posts => {
    const postsForExport = posts.map(post => {
        const { backlinks, author, ...postForExport } = post; // omit backlinks and author
        
        return postForExport;
    });
    jsonExport(postsForExport, {
        headers: ['name', 'roleName', 'statistics.totalTasks', 'statistics.completedTasks'] // order fields in the export
    }, (err, csv) => {
        downloadCSV(csv, 'users'); // download as 'posts.csv` file
    });
};*/

const AvgLevelOfConfidence = ({record, source}) => (
    <Typography>{record[source] == -1 ? 'NA' : record[source]}</Typography>
);
const PostPagination = props => <Pagination rowsPerPageOptions={[25, 50, 100]} {...props} />

 const UserList = ({ translate,...props }) => (
    <List perPage={25} sort={{ field: 'name', order: 'ASC' }} {...props} exporter={exporter}  filters={<UserFilter />} pagination={<PostPagination />}>
        <Datagrid rowClick="show">
            <TextField source="name" label='resources.users.username' />
            <TextField source="totalTasks" label='resources.users.annotations' />
            <TextField source="remainTasks" label='resources.users.pendingTasks' />
            <TextField source="percentage" label='resources.users.percentCompleted' />
            <TextField source="totalTaskDuration" label='resources.users.totalTaskDuration' />
            
            <TextField source="avgTweetTime" label='resources.users.avgTweetTime' />
            <TextField source="totalCategory" label='resources.users.totalCategory' />
            <TextField source="totalDimension" label='resources.users.totalDimension' />
            <TextField source="totalReason" label='resources.users.totalReason' />

        </Datagrid>
    </List>

);
export default UserList;

