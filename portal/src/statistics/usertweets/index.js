import TweetsList from './list'
import UserIcon from '@material-ui/icons/PieChart';

export default {
    list: TweetsList,
    icon: UserIcon,
};
