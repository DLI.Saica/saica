import UserAvgList from './list'
import UserIcon from '@material-ui/icons/PieChart';

export default {
    list: UserAvgList,
    icon: UserIcon,
};
