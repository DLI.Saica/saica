import React, { Component } from "react";
import { GET_LIST,GET_ONE, CREATE, GET_MANY, Responsive, withDataProvider, translate } from 'react-admin';
import compose from 'recompose/compose';
import { connect } from 'react-redux';
import CanvasJSReact from './canvasjs.react';

import {fetchEnd,fetchStart} from 'react-admin';
import { AutocompleteInput, DateInput, ReferenceArrayInput, SelectArrayInput, ReferenceArrayField, SingleFieldList, ChipField, ReferenceInput, ReferenceField, SelectInput, List, Datagrid, TextField, Filter, TextInput, BooleanField, BooleanInput } from 'react-admin';
import WordCloud from './wordCloud';
import Stacked from './Stacked';
import Grid from '@material-ui/core/Grid';
import { Button, Typography } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Multiselect } from 'multiselect-react-dropdown';
import { withStyles } from '@material-ui/core/styles';
import { PieChart } from 'react-minimal-pie-chart';
import Chart from 'react-apexcharts';
import { CSVLink, CSVDownload } from "react-csv";

const styles = {
    flex: {display: 'flex' },
    flexColumn: { display: 'flex', flexDirection: 'column' },
    leftCol: { flex: 1, marginRight: '1em' },
    rightCol: { flex: 1, marginLeft: '1em' },
    singleCol: { marginTop: '2em', marginBottom: '2em' },
};


class Overview extends Component {
    
    state = {lineChart:[], selectReport: 1,UserData:[],
        users: [], selectedUser: [], selected: [], tasksLine:[], open: false, sort: { field: 'category_agreement', order: 'asc' }
    };
    constructor(props) {
        super(props);
        this.onSelect = this.onSelect.bind(this);
        this.onRemove = this.onSelect.bind(this);

    }

    onSelect(selectedList, selectedItem) {
        this.setState({ selectedUser: selectedList });
    }
    onRemove(selectedList, removedItem) {
        this.setState({ selectedUser: selectedList });
    }

    componentDidMount() {
        this.fetchUsers()
    }

    handleChange2 = event => {
        this.fetchUsersValues();
    };
    handleChangeReport = event => {
        this.setState({ selectReport: event.target.value});
    };

    async fetchUsersValues() {
        const { dataProvider } = this.props;
        var x =[];
        if(this.state.selectedUser.length>0){
            for (let e in this.state.selectedUser) 
                {
                    x.push(this.state.selectedUser[e].id)
                }
        }else{
            for (let e in this.state.users) 
                {
                    x.push(this.state.users[e].id)
                }
        }
        x.push(this.state.selectReport);
        const { data: lineChart } = await dataProvider(GET_MANY, 'usersCharts',
            {   ids: x, });
        this.setState({ lineChart });
    }

    async fetchUsers() {
        const { dataProvider } = this.props;
        const { data: users } = await dataProvider(GET_LIST, 'users',
            {
                filter: {role:30},
                sort: { field: 'id', order: 'ASC' },
                pagination: { page: 1, perPage: 100 }
            });
        this.setState({ users });
    }

    render() {
        const { ...props } = this.props;
        props.setFilters = this.setFilters
        const {tasksLine} = this.state;
        const { classes } = this.props;
        return (
            <div>
                <div style={{ marginBottom: '20px', marginTop: '10px' }}>
                    
                    <FormControl variant="outlined" style={{marginRight: '20px', marginLeft: '20px'}}>
                        <Multiselect
                            options={this.state.users} // Options to display in the dropdown
                            selectedValues={this.state.selectedUser} // Preselected value to persist in dropdown
                            onSelect={this.onSelect} // Function will trigger on select event
                            onRemove={this.onRemove} // Function will trigger on remove event
                            displayValue="name" // Property name to display in the dropdown options
                            closeIcon="cancel"/>
                    </FormControl>
                    <FormControl variant="outlined" style={{marginRight: '20px', marginLeft: '20px'}}>
                        <InputLabel htmlFor="ReportType">Report</InputLabel>
                        <Select id="ReportType"
                            native
                            value={this.state.selectReport}
                            onChange={this.handleChangeReport}
                            inputProps={{
                                name: 'name',
                                id: 'id',
                            }}
                        >
                                return (
                                    <option key='1' value='1'>مجموع التويت</option>
                                    <option key='2' value='2'>معدل الثقة</option>
                                    <option key='3' value='3'>معدل الوقت المصروف</option>
                                    <option key='4' value='4'>سحابة الكلمات</option>
                                );
                        </Select>
                    </FormControl>
                    <Button onClick={this.handleChange2} color="primary">Draw</Button>
                    <CSVLink data={this.state.UserData}>Download me</CSVLink>

                </div> 
                <div style={{width:'100%',margin: 'auto'}}>
                    <div className={styles.root} style={{width:'80%', margin: 'auto'}}>
                        {this.chartRender()}
                    </div>
                </div>
            </div>
        );
    }
    
    chartRender(){
        var lines = this.state.lineChart;
        var tempTitle = "";
        if(this.state.selectReport == 1){
            tempTitle="مجموع التويت"
        }else if(this.state.selectReport == 2){
            tempTitle="معدل الثقة"
        }else if(this.state.selectReport == 3){
            tempTitle="معدل الوقت المصروف"
        }

        var tempY = "";
        if(this.state.selectReport == 1){
            tempY=" التويت"
        }else if(this.state.selectReport == 2){
            tempY=" الثقة"
        }else if(this.state.selectReport == 3){
            tempY=" الوقت المصروف"
        }
        
        /*const options = {
            animationEnabled: true,	
            title:{tempTitle},
            axisY : {
                title: tempY},
            toolTip: {
                shared: true
            },
            data: lines
        }
        
       0: {id: 70, type: "spline", name: "Mehdi Lachheb", howInLegend: true, dataPoints: Array(3), …}
       1: {id: 71, type: "spline", name: "Chaouki Mbarki", howInLegend: true, dataPoints: Array(24), …}   
series: [
        {
          name: "High - 2013",
          data: [28, 29, 33, 36, 32, 32, 33]
        },
        {
          name: "Low - 2013",
          data: [12, 11, 14, 18, 17, 13, 13]
        }
      ]*/
       var values=[];
       var xCat=[];
       var minn=0;
       var maxx=20;

       if(this.state.lineChart.length>0 && this.state.selectReport != 4){
        minn=1000;
        maxx=0;
            values = this.state.lineChart.map(function(item) {
                return {name: item.name, data: 
                    item.dataPoints.map(function(item2) {
                        if(minn>item2.data)
                            minn=item2.data;
                        if(maxx<item2.data)
                            maxx=item2.data;
                        return item2.data;})
                };
            });
            
            this.state.lineChart.forEach(element => {
                element.dataPoints.forEach(element2 => {
                    xCat.push(element2.label);
                });
            });
            console.log("xCat");
            console.log(xCat);
        }
    

    var series =  values;
          var options= {
            chart: {
              height: 350,
              type: 'line',
              dropShadow: {
                enabled: true,
                color: '#000',
                top: 18,
                left: 7,
                blur: 10,
                opacity: 0.2
              },
              toolbar: {
                show: true
              }
            },
            colors: ['#77B6EA', '#545454'],
            dataLabels: {
              enabled: true,
            },
            stroke: {
              curve: 'smooth'
            },
            title: {
              text: tempTitle,
              align: 'center'
            },
            grid: {
              borderColor: '#e7e7e7',
              row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
              },
            },
            markers: {
              size: 1
            },
            xaxis: {
              categories: xCat,
              title: {
                text: 'الفترات'
              }
            },
            yaxis: {
              title: {
                text: tempY
              },
              min: minn,
              max: maxx
            },
            legend: {
              position: 'top',
              horizontalAlign: 'right',
              floating: true,
              offsetY: -25,
              offsetX: -5
            }
          };

        if (this.state.selectReport == 4){
            return (
                <Grid item xs={12} sm={12}>
                    <WordCloud data={lines} />
                </Grid>
            );
        }else{
        return (
            <div>
                <Chart options={options} title="xxx" series={series} type="line" width="100%" height="500"/>
            </div>
        );
    }
    }
}

const mapStateToProps = state => ({
    version: state.admin.ui.viewVersion,
});

export default compose(
    connect(mapStateToProps),
    withDataProvider
)(Overview);
