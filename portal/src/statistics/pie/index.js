import PieList from './list'
import UserIcon from '@material-ui/icons/PieChart';

export default {
    list: PieList,
    icon: UserIcon,
};
