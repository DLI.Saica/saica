import React, { Component } from "react";
import { GET_LIST,GET_ONE, CREATE, GET_MANY, Responsive, withDataProvider, translate } from 'react-admin';
import compose from 'recompose/compose';
import { connect } from 'react-redux';
import {fetchEnd,fetchStart} from 'react-admin';
import { AutocompleteInput, DateInput, ReferenceArrayInput, SelectArrayInput, ReferenceArrayField, SingleFieldList, ChipField, ReferenceInput, ReferenceField, SelectInput, List, Datagrid, TextField, Filter, TextInput, BooleanField, BooleanInput } from 'react-admin';
import Statistics from './Statistics';
import Stacked from './Stacked';
import Grid from '@material-ui/core/Grid';
import { Button, Typography } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withStyles } from '@material-ui/core/styles';
import ApexCharts from 'apexcharts'
import Chart from 'react-apexcharts'


const styles = {
    flex: {display: 'flex' },
    flexColumn: { display: 'flex', flexDirection: 'column' },
    leftCol: { flex: 1, marginRight: '1em' },
    rightCol: { flex: 1, marginLeft: '1em' },
    singleCol: { marginTop: '2em', marginBottom: '2em' },
};


class Overview extends Component {
    state = {
        category: [], selectcat: [], selected: [], tasksLine:[], open: false, sort: { field: 'category_agreement', order: 'asc' }
    };
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.fetchCategories()
    }
    async fetchCategories() {
        const { dataProvider } = this.props;
        const { data: category } = await dataProvider(GET_LIST, 'category',
            {
                filter: { categoryId: this.state.selectcat },
                sort: { field: 'name', order: 'ASC' },
                pagination: { page: 1, perPage: 100 }
            });
        this.setState({ category });
    }

    handleChange1 = event => {
        this.setState({ selectcat: event.target.value });
        if (this.state.selectcat) {
            console.log(">>>>>>>>>"+this.state.selectcat)
            console.log(">>>>>>>>>"+event.target.value)
            this.fetchSubCat(event.target.value)
        }
    };

    // handleCompareClick = () => {
    //     if (this.state.selectcat) {
    //         this.fetchSubCat()
    //     }
    // };

    async fetchSubCat(iddd) {
        const { dataProvider } = this.props;
        console.log(">>>>>>>>>>>>>>>>>>"+this.state.selectcat)
        fetchStart();
        const { data : categoryPie } = await dataProvider(
        GET_LIST,
        'subCategory',
        {
            filter: { id: iddd },
            sort: { field: 'name', order: 'ASC' },
            pagination: { page: 1, perPage: 100 }
        }).finally(() => {
            fetchEnd();
        });
        this.setState({categoryPie});
    };



   
    render() {
        const { ...props } = this.props;
        props.setFilters = this.setFilters
        const {tasksLine} = this.state;
        const { classes } = this.props;
        return (
            <div>
                <div style={{ marginBottom: '20px', marginTop: '10px' }}>
                    <FormControl variant="outlined">
                        <InputLabel htmlFor="selectfirstuser">Category</InputLabel>
                        <Select
                            native
                            value={this.state.selectcat}
                            onChange={this.handleChange1}

                            inputProps={{
                                name: 'name',
                                id: 'id',
                            }}
                        >
                            <option value=''></option>
                            {this.state.category.map(({ id, name }) => {
                                return (
                                    <option key={id} value={id}>{name}</option>
                                );
                            })}
                        </Select>
                    </FormControl>
                </div> 
                <div style={{width:'100%'}}>
                    <div className={styles.root} style={{width:800, margin: 'auto'}}>
                    
                    {this.chartRender()}
                        
                    </div>
                </div>
            </div>
        );
    }
    
    chartRender(){
        const defaultLabelStyle = {
            fontSize: '5px',
            fontFamily: 'sans-serif',
          };
          var options= {
            chart: {
                width: 600,
                type: 'pie',
              },
              labels: [],
              responsive: [{
                breakpoint: 600,
                options: {
                  chart: {
                    width: 600
                  },
                  legend: {
                    position: 'bottom'
                  }
                }
              }]
            };
          var series= [];
        if (this.state.categoryPie){
            var titles = this.state.categoryPie.map(function(item) {
                return item.title;
            });
            var values = this.state.categoryPie.map(function(item) {
                return item.value;
            });
            series = values;
            options= {
                chart: {
                    width: 600,
                    type: 'pie',
                  },
                  labels: titles,
                  responsive: [{
                    breakpoint: 600,
                    options: {
                      chart: {
                        width: 600
                      },
                      legend: {
                        position: 'bottom'
                      }
                    }
                  }]
                };
        }
        const { ...props } = this.props;
        props.setFilters = this.setFilters
        const {tasksLine} = this.state;
        const { classes } = this.props;
        const shiftSize = 7;
            return (
                <Chart options={options} series={series} type="pie" width="600" />
                );
        return '';
    }
}



const mapStateToProps = state => ({
    version: state.admin.ui.viewVersion,
});

export default compose(
    connect(mapStateToProps),
    withDataProvider
)(Overview);
