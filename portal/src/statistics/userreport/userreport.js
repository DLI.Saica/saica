import React, { Component } from "react";
import { GET_LIST,GET_ONE, CREATE, GET_MANY, Responsive, withDataProvider, translate } from 'react-admin';
import compose from 'recompose/compose';
import { connect } from 'react-redux';
import CanvasJSReact from './canvasjs.react';

import {fetchEnd,fetchStart} from 'react-admin';
import { AutocompleteInput, DateInput, ReferenceArrayInput, SelectArrayInput, ReferenceArrayField, SingleFieldList, ChipField, ReferenceInput, ReferenceField, SelectInput, List, Datagrid, TextField, Filter, TextInput, BooleanField, BooleanInput } from 'react-admin';
import WordCloud from './wordCloud';
import Stacked from './Stacked';
import Grid from '@material-ui/core/Grid';
import { Button, Typography } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Multiselect } from 'multiselect-react-dropdown';
import { withStyles } from '@material-ui/core/styles';
import { PieChart } from 'react-minimal-pie-chart';
import CsvDownloader from 'react-csv-downloader';

const styles = {
    flex: {display: 'flex' },
    flexColumn: { display: 'flex', flexDirection: 'column' },
    leftCol: { flex: 1, marginRight: '1em' },
    rightCol: { flex: 1, marginLeft: '1em' },
    singleCol: { marginTop: '2em', marginBottom: '2em' },
};



class Overview extends Component {
    state = {UserData1:[], UserData2:[], UserData3:[], UserData4:[] };
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.fetchUserData1();
        this.fetchUserData2();
        this.fetchUserData3();
        this.fetchUserData4();
    }

    async fetchUserData1() {
        const { dataProvider } = this.props;
        const { data: UserData1 } = await dataProvider(GET_LIST, 'userData',
            {
                filter: { type: 1},
                sort: { field: 'id', order: 'ASC' },
                pagination: { page: 1, perPage: 100 }
            });
        this.setState({ UserData1 });
    }

    async fetchUserData2() {
        const { dataProvider } = this.props;
        const { data: UserData2 } = await dataProvider(GET_LIST, 'userData',
            {
                filter: { type: 2},
                sort: { field: 'id', order: 'ASC' },
                pagination: { page: 1, perPage: 100 }
            });
        this.setState({ UserData2 });
    }
    async fetchUserData3() {
        const { dataProvider } = this.props;
        const { data: UserData3 } = await dataProvider(GET_LIST, 'userData',
            {
                filter: { type: 3},
                sort: { field: 'id', order: 'ASC' },
                pagination: { page: 1, perPage: 100 }
            });
        this.setState({ UserData3 });
    }
    async fetchUserData4() {
        const { dataProvider } = this.props;
        const { data: UserData4 } = await dataProvider(GET_LIST, 'userData',
            {
                filter: { type: 4},
                sort: { field: 'id', order: 'ASC' },
                pagination: { page: 1, perPage: 100 }
            });
        this.setState({ UserData4 });
    }
    
    render() {
        const { ...props } = this.props;
        const columns1 = [{
            id: 'name',
            displayName: 'Annotator Name'
          }, {
            id: 'noCompleted',
            displayName: 'Finished Tweets'
          },
          {
            id: 'totalTime',
            displayName: 'Total Time Annotating in Minutes'
          },
          {
            id: 'avgTimeTweet',
            displayName: 'Average Time Annotating'
          }, {
            id: 'avgCountCategory',
            displayName: 'Average Category Used Per Tweet'
          },  {
            id: 'avgCountDimention',
            displayName: 'Average Dimention Used Per Tweet'
          },
        ]
        const columns3 = [{
            id: 'noAnnotatedTweet',
            displayName: 'Count Tweets'
          }, {
            id: 'categoryAr',
            displayName: 'Category Name Arabic'
          },
          {
            id: 'categoryEn',
            displayName: 'Category Name English'
          },
          {
            id: 'dimentionAr',
            displayName: 'Dimention Name Arabic'
          },
          {
            id: 'dimentionEn',
            displayName: 'Dimention Name English'
          }
        ]

        const columns4 = [{
            id: 'word',
            displayName: 'Word'
          }, {
            id: 'countWord',
            displayName: 'Word Count'
          }
        ]
        return (
            <div>
                <div style={{ marginBottom: '20px', marginTop: '10px' }}>

                <CsvDownloader
                    filename="PerformanceSummary"
                    columns={columns1}
                    datas={this.state.UserData1}
                    text="Performance Summary" />

                <CsvDownloader
                    filename="SummeryWeek"
                    datas={this.state.UserData2}
                    text="Performance Summary Week" />

                <CsvDownloader
                    filename="SummaryTweetsAnnotated"
                    columns={columns3}
                    datas={this.state.UserData3}
                    text="Summary Tweets Annotated" />

                <CsvDownloader
                    filename="TopList"
                    columns={columns4}
                    datas={this.state.UserData4}
                    text="Top List" />
                </div> 
                
            </div>
        );
    }
}

const mapStateToProps = state => ({
    version: state.admin.ui.viewVersion,
});

export default compose(
    connect(mapStateToProps),
    withDataProvider
)(Overview);
