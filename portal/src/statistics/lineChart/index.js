import CloudList from './list'
import UserIcon from '@material-ui/icons/PieChart';

export default {
    list: CloudList,
    icon: UserIcon,
};
