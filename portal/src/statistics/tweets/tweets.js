import React, { Component } from "react";
import { GET_LIST,GET_ONE, CREATE, GET_MANY, Responsive, withDataProvider, translate } from 'react-admin';
import compose from 'recompose/compose';
import { connect } from 'react-redux';
import CanvasJSReact from './canvasjs.react';

import {fetchEnd,fetchStart} from 'react-admin';
import { AutocompleteInput, DateInput, ReferenceArrayInput, SelectArrayInput, ReferenceArrayField, SingleFieldList, ChipField, ReferenceInput, ReferenceField, SelectInput, List, Datagrid, TextField, Filter, TextInput, BooleanField, BooleanInput } from 'react-admin';
import WordCloud from './wordCloud';
import Stacked from './Stacked';
import Grid from '@material-ui/core/Grid';
import { Button, Typography } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Multiselect } from 'multiselect-react-dropdown';
import { withStyles } from '@material-ui/core/styles';
import { PieChart } from 'react-minimal-pie-chart';
import Chart from 'react-apexcharts'

const styles = {
    flex: {display: 'flex' },
    flexColumn: { display: 'flex', flexDirection: 'column' },
    leftCol: { flex: 1, marginRight: '1em' },
    rightCol: { flex: 1, marginLeft: '1em' },
    singleCol: { marginTop: '2em', marginBottom: '2em' },
};


class Overview extends Component {
    
    state = {lineChart:[],
        category: [], dimension: [], selectcat: [], selectTime: 1,  selectDim: [], selected: [], tasksLine:[], open: false, sort: { field: 'category_agreement', order: 'asc' }
    };
    constructor(props) {
        super(props);
        this.onSelect = this.onSelect.bind(this);
        this.onRemove = this.onSelect.bind(this);

    }

    onSelect(selectedList, selectedItem) {
        this.setState({ selectDim: selectedList });
    }
    onRemove(selectedList, removedItem) {
        this.setState({ selectDim: selectedList });
    }

    componentDidMount() {
        this.fetchCategories()
    }
    async fetchCategories() {
        const { dataProvider } = this.props;
        const { data: category } = await dataProvider(GET_LIST, 'category',
            {
                filter: { categoryId: this.state.selectcat },
                sort: { field: 'name', order: 'ASC' },
                pagination: { page: 1, perPage: 100 }
            });
        this.setState({ category });
    }
    
    
    handleChange2 = event => {
        this.fetchDimValues();
        
    };
    handleChangeTime = event => {
        this.setState({ selectTime: event.target.value});
    };

    handleChange1 = event => {
        this.setState({ selectcat: event.target.value });
        this.fetchDimension(event.target.value)
    };

    async fetchDimValues() {
        const { dataProvider } = this.props;
console.log(this.state.selectDim);
var x =[];
for (let e in this.state.selectDim) 
    {
        x.push(this.state.selectDim[e].id)
        
    }
    x.push(this.state.selectTime);

        const { data: lineChart } = await dataProvider(GET_MANY, 'dimensionLine',
            {   ids: x,
            });
        this.setState({ lineChart });
        console.log(lineChart);
    }

    async fetchDimension(iddd) {
        const { dataProvider } = this.props;
        const { data: dimension } = await dataProvider(GET_LIST, 'dimension',
            {
                filter: { categoryId: iddd },
                sort: { field: 'name', order: 'ASC' },
                pagination: { page: 1, perPage: 100 }
            });
        this.setState({ dimension });
    }

    handleClose = () => {
        this.setState({ open: false });
    };

    async fetchSubCat(iddd) {
        const { dataProvider } = this.props;
        fetchStart();
        const { data : categoryPie } = await dataProvider(
        GET_LIST,
        'subCategoryCloud',
        {
            filter: { id: iddd },
            sort: { field: 'name', order: 'ASC' },
            pagination: { page: 1, perPage: 100 }
        }).finally(() => {
            fetchEnd();
        });

        this.setState({categoryPie});
    };

    render() {
        const { ...props } = this.props;
        props.setFilters = this.setFilters
        const {tasksLine} = this.state;
        const { classes } = this.props;
        return (
            <div>
                <div style={{ marginBottom: '20px', marginTop: '10px' }}>
                    <FormControl variant="outlined" style={{marginRight: '20px', marginLeft: '20px'}}>
                        <InputLabel htmlFor="CategorySelect">Category</InputLabel>
                        <Select id="CategorySelect"
                            native
                            value={this.state.selectcat}
                            onChange={this.handleChange1}

                            inputProps={{
                                name: 'name',
                                id: 'id',
                            }}
                        >
                            <option value=''></option>
                            {this.state.category.map(({ id, name }) => {
                                return (
                                    <option key={id} value={id}>{name}</option>
                                );
                            })}
                        </Select>
                    </FormControl>
                    <FormControl variant="outlined" style={{marginRight: '20px', marginLeft: '20px'}}>
                        <Multiselect
                            options={this.state.dimension} // Options to display in the dropdown
                            selectedValues={this.state.selectDim} // Preselected value to persist in dropdown
                            onSelect={this.onSelect} // Function will trigger on select event
                            onRemove={this.onRemove} // Function will trigger on remove event
                            displayValue="name" // Property name to display in the dropdown options
                            closeIcon="cancel"/>
                    </FormControl>
                    <FormControl variant="outlined" style={{marginRight: '20px', marginLeft: '20px'}}>
                        <InputLabel htmlFor="TimeSelect">Time</InputLabel>
                        <Select id="TimeSelect"
                            native
                            value={this.state.selectTime}
                            onChange={this.handleChangeTime}
                            inputProps={{
                                name: 'name',
                                id: 'id',
                            }}
                        >
                                return (
                                    <option key='1' value='1'>Day</option>
                                    <option key='2' value='2'>Week</option>
                                    <option key='3' value='3'>Month</option>
                                );
                        </Select>
                    </FormControl>
                    <Button onClick={this.handleChange2} color="primary">Draw</Button>
                </div> 
                <div style={{width:'100%',margin: 'auto'}}>
                    <div className={styles.root} style={{width:'80%', margin: 'auto'}}>
                        {this.chartRender()}
                    </div>
                </div>
            </div>
        );
    }
    
    chartRender(){
        var values=[];
       var xCat=[];
       var minn=0;
       var maxx=20;

       if(this.state.lineChart.length>0){
        minn=1000;
        maxx=0;
            values = this.state.lineChart.map(function(item) {
                return {name: item.name, data: 
                    item.dataPoints.map(function(item2) {
                        if(minn>item2.data)
                            minn=item2.data;
                        if(maxx<item2.data)
                            maxx=item2.data;
                        return item2.data;})
                };
            });
            
            this.state.lineChart.forEach(element => {
                element.dataPoints.forEach(element2 => {
                    xCat.push(element2.label);
                });
            });
            console.log("xCat");
            console.log(xCat);
        }
    

    var series =  values;
          var options= {
            chart: {
              height: 350,
              type: 'line',
              dropShadow: {
                enabled: true,
                color: '#000',
                top: 18,
                left: 7,
                blur: 10,
                opacity: 0.2
              },
              toolbar: {
                show: true
              }
            },
            colors: ['#77B6EA', '#545454'],
            dataLabels: {
              enabled: true,
            },
            stroke: {
              curve: 'smooth'
            },
            title: {
              text: 'Average High & Low Temperature',
              align: 'right'
            },
            grid: {
              borderColor: '#e7e7e7',
              row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
              },
            },
            markers: {
              size: 1
            },
            xaxis: {
              categories: xCat,
              title: {
                text: 'الفترات'
              }
            },
            yaxis: {
              title: {
                text: 'مجموع التويت'
              },
              min: minn,
              max: maxx
            },
            legend: {
              position: 'top',
              horizontalAlign: 'right',
              floating: true,
              offsetY: -25,
              offsetX: -5
            }
          };
          return (
            <div>
                <Chart options={options} series={series} type="line" width="100%" height="500" />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    version: state.admin.ui.viewVersion,
});

export default compose(
    connect(mapStateToProps),
    withDataProvider
)(Overview);
