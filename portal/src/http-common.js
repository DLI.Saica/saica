import axios from "axios";

export default axios.create({
  baseURL: "http://207.180.223.162:8888",
  headers: {
    "Content-type": "application/json"
  }
});