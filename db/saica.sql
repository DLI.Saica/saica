-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 02, 2021 at 12:33 PM
-- Server version: 8.0.23-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saica4`
--

-- --------------------------------------------------------

--
-- Table structure for table `annotation`
--

CREATE TABLE `annotation` (
  `Id` int NOT NULL,
  `CreationDate` datetime NOT NULL,
  `LastModified` datetime NOT NULL,
  `IsDeleted` tinyint(1) NOT NULL,
  `Status` int NOT NULL,
  `StartTime` datetime NOT NULL,
  `FinishTime` datetime DEFAULT NULL,
  `AnnotationTaskId` int NOT NULL,
  `CategoryId` int NOT NULL,
  `DimensionId` int NOT NULL,
  `AnnotationTaskUserTweetId` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `annotationreason`
--

CREATE TABLE `annotationreason` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `AnnotationId` int DEFAULT NULL,
  `CategoryId` int DEFAULT NULL,
  `DimensionId` int DEFAULT NULL,
  `StartWordId` int DEFAULT NULL,
  `EndWordId` int DEFAULT NULL,
  `StartWordPosition` int DEFAULT NULL,
  `EndWordPosition` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `annotationreasonword`
--

CREATE TABLE `annotationreasonword` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` bit(1) DEFAULT NULL,
  `AnnotationReasonId` int DEFAULT NULL,
  `TweetWordId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `annotationtask`
--

CREATE TABLE `annotationtask` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `Name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `StartTweetId` int DEFAULT NULL,
  `EndTweetId` int DEFAULT NULL,
  `CreatedByUserId` int DEFAULT NULL,
  `IsFinished` tinyint(1) DEFAULT NULL,
  `UserId` int DEFAULT NULL,
  `Status` int DEFAULT NULL,
  `StartTime` datetime DEFAULT NULL,
  `FinishTime` datetime DEFAULT NULL,
  `TaskDuration` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `annotationtaskuser`
--

CREATE TABLE `annotationtaskuser` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `UserId` int DEFAULT NULL,
  `AnnotationTaskId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `annotationtaskusertweet`
--

CREATE TABLE `annotationtaskusertweet` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `TweetId` int DEFAULT NULL,
  `UserId` int DEFAULT NULL,
  `AnnotationTaskId` int DEFAULT NULL,
  `Status` int DEFAULT NULL,
  `StartTime` datetime DEFAULT NULL,
  `FinishTime` datetime DEFAULT NULL,
  `IsIrrelevant` tinyint(1) DEFAULT NULL,
  `LevelOfConfidenceId` int DEFAULT NULL,
  `TaskDuration` int DEFAULT NULL,
  `lastAction` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Triggers `annotationtaskusertweet`
--
DELIMITER $$
CREATE TRIGGER `AfterUpdateTask` AFTER UPDATE ON `annotationtaskusertweet` FOR EACH ROW IF NEW.Status=30 
THEN 
INSERT INTO saica4.ALL_FINISHED(duration, finishedTime, userId, tweetId, LevelOfConfidenceId, `WEEK`, `DAY`, `MONTH`, Category, Dimension, wordId) 
select NEW.TaskDuration, NOW(), NEW.UserId, NEW.TweetId, NEW.LevelOfConfidenceId,WEEK(now()),DAY(now()),MONTH(now()),
CategoryId, DimensionId , saica4.getWordId(StartWordId) from saica4.annotationreason_report
where IsDeleted=0 and userId=NEW.UserId and AnnotationId in (select id from saica4.annotation_report where AnnotationTaskUserTweetId=NEW.ID and IsDeleted=0);
END IF
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `audit`
--

CREATE TABLE `audit` (
  `Id` int NOT NULL,
  `CreationDate` datetime NOT NULL,
  `LastModified` datetime NOT NULL,
  `IsDeleted` bit(1) DEFAULT NULL,
  `Message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `Ip` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `LogType` int DEFAULT NULL,
  `UserAgent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `Url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `AuditType` int DEFAULT NULL,
  `CreatedByUserId` int DEFAULT NULL,
  `AffectedUserId` int DEFAULT NULL,
  `TweetId` int DEFAULT NULL,
  `AnnotationId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT '0',
  `Name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `DisplayOrder` int DEFAULT NULL,
  `Icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT '1',
  `NameEn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dataset`
--

CREATE TABLE `dataset` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dimension`
--

CREATE TABLE `dimension` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `DisplayOrder` int DEFAULT NULL,
  `Icon` varchar(255) DEFAULT NULL,
  `Color` varchar(255) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT NULL,
  `CategoryId` int DEFAULT NULL,
  `NameEn` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `levelofconfidence`
--

CREATE TABLE `levelofconfidence` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `DisplayOrder` int DEFAULT NULL,
  `Icon` varchar(255) DEFAULT NULL,
  `Color` varchar(255) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT NULL,
  `NameEn` varchar(255) DEFAULT NULL,
  `Value` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` bit(1) DEFAULT NULL,
  `Title` text,
  `CreatedByUserId` int DEFAULT NULL,
  `CreatedForUserId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `Id` int NOT NULL,
  `CreationDate` datetime NOT NULL,
  `LastModified` datetime NOT NULL,
  `IsDeleted` bit(1) DEFAULT NULL,
  `Name` text,
  `IsActive` bit(1) DEFAULT NULL,
  `DisplayOrder` int NOT NULL,
  `RoleKey` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `idtest` int NOT NULL,
  `timeupdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tweet`
--

CREATE TABLE `tweet` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT '0',
  `TweetCreationDate` datetime DEFAULT NULL,
  `TweetId` bigint DEFAULT NULL,
  `Text` longtext,
  `DatasetId` int DEFAULT NULL,
  `AccountDisplayName` varchar(255) DEFAULT NULL,
  `lang` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tweetaccount`
--

CREATE TABLE `tweetaccount` (
  `Id` bigint NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `UserName` text,
  `Url` text,
  `Photo` text,
  `DisplayName` text,
  `PublicId` int DEFAULT NULL,
  `DisplayOrder` int DEFAULT NULL,
  `IsActive` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tweetword`
--

CREATE TABLE `tweetword` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `TweetId` int DEFAULT NULL,
  `WordId` int DEFAULT NULL,
  `Position` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `Id` int NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `CreationDate` datetime NOT NULL,
  `LastModified` datetime NOT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `PasswordHash` varchar(255) NOT NULL,
  `PasswordSalt` varchar(255) NOT NULL,
  `Role` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `userrole`
--

CREATE TABLE `userrole` (
  `Id` int NOT NULL,
  `CreationDate` datetime NOT NULL,
  `LastModified` datetime NOT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `UserId` int NOT NULL,
  `RoleId` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `word`
--

CREATE TABLE `word` (
  `Id` int NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `Name` longtext,
  `Language` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `annotation`
--
ALTER TABLE `annotation`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_annotation_AnnotationTaskId` (`AnnotationTaskId`),
  ADD KEY `IX_annotation_AnnotationTaskUserTweetId` (`AnnotationTaskUserTweetId`);

--
-- Indexes for table `annotationreason`
--
ALTER TABLE `annotationreason`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_annotationreason_AnnotationId` (`AnnotationId`),
  ADD KEY `IX_annotationreason_CategoryId` (`CategoryId`),
  ADD KEY `IX_annotationreason_DimensionId` (`DimensionId`),
  ADD KEY `IX_annotationreason_StartWordId` (`StartWordId`);

--
-- Indexes for table `annotationreasonword`
--
ALTER TABLE `annotationreasonword`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_annotationreasonword_AnnotationReasonId` (`AnnotationReasonId`),
  ADD KEY `IX_annotationreasonword_TweetWordId` (`TweetWordId`);

--
-- Indexes for table `annotationtask`
--
ALTER TABLE `annotationtask`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AnnotationTask_CreatedByUserId` (`CreatedByUserId`);

--
-- Indexes for table `annotationtaskuser`
--
ALTER TABLE `annotationtaskuser`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AnnotationTaskUser_AnnotationTaskId` (`AnnotationTaskId`),
  ADD KEY `IX_AnnotationTaskUser_UserId` (`UserId`);

--
-- Indexes for table `annotationtaskusertweet`
--
ALTER TABLE `annotationtaskusertweet`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AnnotationTaskUserTweet_AnnotationTaskId` (`AnnotationTaskId`),
  ADD KEY `IX_AnnotationTaskUserTweet_LevelOfConfidenceId` (`LevelOfConfidenceId`),
  ADD KEY `IX_AnnotationTaskUserTweet_TweetId` (`TweetId`),
  ADD KEY `IX_AnnotationTaskUserTweet_UserId` (`UserId`);

--
-- Indexes for table `audit`
--
ALTER TABLE `audit`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Audit_CreatedByUserId` (`CreatedByUserId`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `dataset`
--
ALTER TABLE `dataset`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `dimension`
--
ALTER TABLE `dimension`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Dimension_CategoryId` (`CategoryId`);

--
-- Indexes for table `levelofconfidence`
--
ALTER TABLE `levelofconfidence`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Notification_CreatedByUserId` (`CreatedByUserId`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`idtest`);

--
-- Indexes for table `tweet`
--
ALTER TABLE `tweet`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Tweet_DatasetId` (`DatasetId`);

--
-- Indexes for table `tweetaccount`
--
ALTER TABLE `tweetaccount`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tweetword`
--
ALTER TABLE `tweetword`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_TweetWord_TweetId` (`TweetId`),
  ADD KEY `IX_TweetWord_WordId` (`WordId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `userrole`
--
ALTER TABLE `userrole`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_UserRole_RoleId` (`RoleId`),
  ADD KEY `IX_UserRole_UserId` (`UserId`);

--
-- Indexes for table `word`
--
ALTER TABLE `word`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `annotation`
--
ALTER TABLE `annotation`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `annotationreason`
--
ALTER TABLE `annotationreason`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `annotationreasonword`
--
ALTER TABLE `annotationreasonword`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `annotationtask`
--
ALTER TABLE `annotationtask`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `annotationtaskuser`
--
ALTER TABLE `annotationtaskuser`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `annotationtaskusertweet`
--
ALTER TABLE `annotationtaskusertweet`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `audit`
--
ALTER TABLE `audit`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dataset`
--
ALTER TABLE `dataset`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dimension`
--
ALTER TABLE `dimension`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `levelofconfidence`
--
ALTER TABLE `levelofconfidence`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `idtest` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userrole`
--
ALTER TABLE `userrole`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `annotation`
--
ALTER TABLE `annotation`
  ADD CONSTRAINT `FK_annotation_AnnotationTask_AnnotationTaskId` FOREIGN KEY (`AnnotationTaskId`) REFERENCES `annotationtask` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_annotation_AnnotationTaskUserTweet_AnnotationTaskUserTweetId` FOREIGN KEY (`AnnotationTaskUserTweetId`) REFERENCES `annotationtaskusertweet` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `annotationreason`
--
ALTER TABLE `annotationreason`
  ADD CONSTRAINT `FK_annotationreason_annotation_AnnotationId` FOREIGN KEY (`AnnotationId`) REFERENCES `annotation` (`Id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `FK_annotationreason_Category_CategoryId` FOREIGN KEY (`CategoryId`) REFERENCES `category` (`id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `FK_annotationreason_Dimension_DimensionId` FOREIGN KEY (`DimensionId`) REFERENCES `dimension` (`id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `FK_annotationreason_TweetWord_StartWordId` FOREIGN KEY (`StartWordId`) REFERENCES `tweetword` (`Id`) ON DELETE RESTRICT;

--
-- Constraints for table `annotationreasonword`
--
ALTER TABLE `annotationreasonword`
  ADD CONSTRAINT `FK_annotationreasonword_annotationreason_AnnotationReasonId` FOREIGN KEY (`AnnotationReasonId`) REFERENCES `annotationreason` (`Id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `FK_annotationreasonword_TweetWord_TweetWordId` FOREIGN KEY (`TweetWordId`) REFERENCES `tweetword` (`Id`) ON DELETE RESTRICT;

--
-- Constraints for table `annotationtask`
--
ALTER TABLE `annotationtask`
  ADD CONSTRAINT `FK_AnnotationTask_User1_CreatedByUserId` FOREIGN KEY (`CreatedByUserId`) REFERENCES `user` (`id`) ON DELETE RESTRICT;

--
-- Constraints for table `annotationtaskuser`
--
ALTER TABLE `annotationtaskuser`
  ADD CONSTRAINT `FK_AnnotationTaskUser_AnnotationTask_AnnotationTaskId` FOREIGN KEY (`AnnotationTaskId`) REFERENCES `annotationtask` (`Id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `FK_AnnotationTaskUser_User1_UserId` FOREIGN KEY (`UserId`) REFERENCES `user` (`id`) ON DELETE RESTRICT;

--
-- Constraints for table `annotationtaskusertweet`
--
ALTER TABLE `annotationtaskusertweet`
  ADD CONSTRAINT `FK_AnnotationTaskUserTweet_AnnotationTask_AnnotationTaskId` FOREIGN KEY (`AnnotationTaskId`) REFERENCES `annotationtask` (`Id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `FK_AnnotationTaskUserTweet_LevelOfConfidence_LevelOfConfidenceId` FOREIGN KEY (`LevelOfConfidenceId`) REFERENCES `levelofconfidence` (`id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `FK_AnnotationTaskUserTweet_Tweet_TweetId` FOREIGN KEY (`TweetId`) REFERENCES `tweet` (`Id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `FK_AnnotationTaskUserTweet_User1_UserId` FOREIGN KEY (`UserId`) REFERENCES `user` (`id`) ON DELETE RESTRICT;

--
-- Constraints for table `audit`
--
ALTER TABLE `audit`
  ADD CONSTRAINT `FK_Audit_User1_CreatedByUserId` FOREIGN KEY (`CreatedByUserId`) REFERENCES `user` (`id`) ON DELETE RESTRICT;

--
-- Constraints for table `dimension`
--
ALTER TABLE `dimension`
  ADD CONSTRAINT `FK_Dimension_Category_CategoryId` FOREIGN KEY (`CategoryId`) REFERENCES `category` (`Id`) ON DELETE RESTRICT;

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `FK_Notification_User1_CreatedByUserId` FOREIGN KEY (`CreatedByUserId`) REFERENCES `user` (`id`) ON DELETE RESTRICT;

--
-- Constraints for table `tweet`
--
ALTER TABLE `tweet`
  ADD CONSTRAINT `FK_Tweet_Dataset_DatasetId` FOREIGN KEY (`DatasetId`) REFERENCES `dataset` (`Id`) ON DELETE RESTRICT;

--
-- Constraints for table `tweetword`
--
ALTER TABLE `tweetword`
  ADD CONSTRAINT `FK_TweetWord_Tweet_TweetId` FOREIGN KEY (`TweetId`) REFERENCES `tweet` (`Id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `FK_TweetWord_Word_WordId` FOREIGN KEY (`WordId`) REFERENCES `word` (`Id`) ON DELETE RESTRICT;

--
-- Constraints for table `userrole`
--
ALTER TABLE `userrole`
  ADD CONSTRAINT `FK_UserRole_Role_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `role` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_UserRole_User1_UserId` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
